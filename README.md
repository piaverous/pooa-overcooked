# Higher Cookery 

Le but de ce projet est d'appliquer les notions du cours en Programmation Orientée Objet Avancée en C# en tâchant de réaliser un jeu reprenant le concept du jeu [Overcooked](https://fr.wikipedia.org/wiki/Overcooked).
Nous détaillerons par la suite le principe du jeu, l'utilisation du projet, la structure que nous avons choisie pour ce projet et ses perspectives.

## Principe du jeu 
### Règles du jeu
Les règles de ce jeu sont relativement simples. Le joueur incarne un cuisinier dans une cuisine
de restaurant, et des clients arrivent avec des commandes, qu'il doit alors préparer. Pour cela, 
le joueur dispose alors d'un temps imparti pour préparer les ingrédient (découpe, cuisson), et
les servir dans un plat pour les livrer aux clients.

Sur chaque niveau, le joueur doit réaliser 5 plats pour les clients. Plus un plat est préparé
rapidement, plus il rapporte des points.

La cuisson est la partie la plus délicate de la préparation, car si l'ingrédient est laissé trop longtemps
sur le feu, il va déclencher un incendie, qu'il faudra contrôler à l'aide d'un extincteur.

### Comment jouer
- **Se déplacer**: On déplace le joueur à l'aide des 4 flèches directionnelles du clavier. 
    Garder *[Left Shift]* enfoncé pour sprinter.
	> N.B. Courir à travers un angle mural provoque une chute terrible et sans fin.
- **Ramasser un objet**: On ramasse un objet en appuyant sur **[C]**. On le porte alors; on ne peut
    porter qu'un objet à la fois. C'est la la plus utilisée outre les déplacements. 
    > N.B. On ne peut poser/ramasser des objets que sur/depuis des tables
- **Réaliser une action**: On utilise la touche **[V]** pour réaliser une action. Exemples d'actions: 
    - Faire apparaître une assiette depuis un spot de génération d'assiettes  
    - Faire apparaître une ingrédient depuis un spot de génération d'ingrédient
    - Garder enfoncé pour découper un ingrédient sur une planche à découper  

### Recettes réalisables
Un certain nombre de recettes sont réalisables dans notre implémentation du jeu:
- **CheeseBurger**: Pain burger + Steak haché puis cuit + Fromage découpé
- **TomatoBurger**: Pain burger + Steak haché puis cuit + Tomate découpée
- **Maxi Burger**: Pain burger + Steak haché puis cuit + Tomate découpée + Fromage découpé
> N.B. Chaque recette doit être servie dans une assiette, et l'assemblage d'ingrédients ne
> peut avoir lieu que dans une assiette.

## Utilisation du projet

Dans le dossier ```Game``` du projet se trouvent les dossiers contenant l'exécutable, pour Windows et MacOS. Double cliquez sur l'exécutable ```HigherCookery``` pour lancer le jeu. Pour quitter le jeu, appuyez sur ```ALT``` + ```F4``` sur Windows, ou ```CMD``` + ```Q``` sur MacOS.

Nous conseillons de faire le tutoriel avant de commencer à jouer, pour une expérience de jeu optimale.
## Structure du projet 

Le propos de cette section est de décrire la structure que nous avons choisie pour réaliser
ce projet et de revenir sur les subtilités de notre architecture et les raisons qui nous 
ont poussé à faire ces choix. Nous détaillerons dans un premier temps la structure globale 
que nous avons adoptée, puis nous détaillerons les spécificités des principaux éléments.

> N.B. Tous les fichiers C#, qui constituent la logique du jeu, sont situés dans ```Assets/Scripts```.

### Structure globale 

Notre objectif en concevant la structure de ce projet était de concevoir quelque chose de 
fonctionnel permettant facilement l'ajout des différentes fonctionnalités prévues au fur 
et à mesure de notre avancement. A cette fin, nous avons distingué différents types d'éléments : 
- l'environnement du jeu, c'est-à-dire les tables et les interactions possibles avec elles. 
- les objets, c'est-à-dire l'ensemble des items que le joueur peut porter, déposer et éventuellement utiliser
- la logique du jeu, c'est-à-dire le contrôle du joueur, ses modes d'interaction avec les autres éléments et le déroulement d'un niveau 

Nous avons donc pu définir une architecture pour ces différents éléments (en particulier les 
deux premiers qui se prêtent très bien à une implémentation reposant sur les principes 
de l'orienté objet). Nous la détaillerons dans les sections suivantes ainsi que les principaux 
challenges que nous avons rencontré en les implémentant et comment nous les avons résolus. 
Enfin nous détaillerons tout particulièrement l'implémentation des évènements asynchrones se déroulant 
au cours du jeu et les contraintes particulières dues au fonctionnement de Unity que nous 
avons dû intégrer.

### Environnement 

Nous avons choisi de faire passer toutes les interactions du joueur avec le jeu par les tables. 
Celles-ci nous permettent d'éviter certains bugs et d'avoir un meilleur contrôle sur les action 
possibles. Il est par exemple impossible de déposer un objet sur le sol, seule une 
table peut l'accueillir. Toutes les tables héritent la classe `Table`, qui définit les 
principales interactions du joueur possibles : déposer un objet, récupérer un objet. 

Toutefois, la méthode PutDown qui fait l'interface entre l'objet tenu par le joueur et la table ne 
se contente pas juste de déposer l'objet : suivant le contenu de la table et l'objet en train d'être manipulé,
elle choisit entre plusieurs comportements (simplement poser l'objet, ajouter l'ingrédient dans un contenant, 
transférer le contenu entre deux containers, etc.).

En plus de cette qualité de stockage, certaines tables peuvent effectuer une action. 
Elles sont divisées en 2 catégories : les tables automatiques et déclenchables.

![Architecture de l'environnement](readme_visuals/Environment_architecture.png)


#### Table automatique 

En héritant de `AutomaticActionTable`, ces tables peuvent transformer un objet dès lors
qu'il y est déposé. C'est le cas de :
- La poubelle : détruit l'aliment ou le plat déposé
- La table de livraison : livre le plat déposé 
- La plaque de cuisson : cuit l'aliment déposé (qui nécessite d'être dans un ```CookingContainerItem```)

#### Table déclenchable 

En héritant de `TriggerableActionTable`, ces tables peuvent transformer un objet 
déposé ou en générer un sous l'action du joueur. C'est le cas de :
- Les tables sources : générent des assietes ou des aliments
- La table de découpe : découpe certains aliments 

#### Barres de progression

Certaines tables implémentent une barre de progression lorsqu'elles effectuent l'action qui 
les définit. L'objet concerné par cette action ne sera transformé qu'après complétion 
de cette barre de progression. C'est le cas de la table de découpe (le joueur doit maintenir 
son action jusqu'à la fin de la découpe) et de la plaque de cuisson (le joueur doit attendre
la fin de la cuisson). Cette dernière implémente une logique supplémentaire : 
une fois l'aliment cuit, une nouvelle barre de progression apparait. Si le joueur 
ne parvient pas à récupérer l'aliment cuit avant sa complétion, l'aliment brûle, 
l'ustensile et la plaque de cuisson s'enflamment...

#### Le feu

Un ingrédient laissé trop longtemps sur une plaque de cuisson mettra le feu à la 
table surlequel il est posé. Le feu pourra ensuite se propager toutes les 5 secondes 
à une table adjacente de la table en feu, la rendant inutilisable jusqu'à extinction 
du feu et brûlant les ingrédients et recettes posées dessus. A cette fin, la prise du 
feu déclenche une Coroutine (voir section sur les évènements asynchrones) visant à propager 
le feu aux tables adjacentes. Lorsque le feu est éteint, la coroutine est interrompue mais 
le feu peut reprendre si il se propage à nouveau depuis une table adjacente.

Lorsqu'un ingrédient ou un contenant non vide se trouve sur une table qui prend feu, 
il est brûlé. il faut alors jeter l'ingrédient ou vider le contenant pour pouvoir recommencer 
à l'utiliser. 


#### Le focus

La détection de tables est possible grâce à un RayCast, un objet propre à Unity. 
Il s'agit d'un rayon vertical que nous avons placé devant le personnage. Dès que ce 
rayon entre en "collision" avec une table, celle-ci rentre dans le "focus" du personnage 
(elle peut en sortir si le rayon n'est plus en collision avec elle). 
Afin de simplifier l'expérience utilisateur, sa texture est éclaircie 
lorsqu'elle est dans le focus. 


### Objet 

La propriété qui définit un objet (ou "item") est sa capacité à être porté par le 
joueur. Chaque objet hérite de la classe `Item`.

![Architecture des objets](readme_visuals/Item_architecture.png)


#### Aliment

Les aliments héritent de la classe `FoodItem`. Un aliment peut avoir jusqu'à trois 
états :
- Cru : peut implément l'interface  `IChoppable` 
- Découpé : peut implémenter l'interface `ICookable`
- Cuit

Le dernier état possible d'un aliment définit la forme sous laquelle il pourra être 
incorporé dans une recette. 

#### Conteneurs

Un conteneur est un objet qui peut contenir un aliment ou une recette (composition 
d'aliments). Tous les conteneurs héritent de `ContainerItem`.
- L'assiette : l'assiette est le seul endroit où une recette peut être construite, en 
déposant les aliments un à un. L'assiette est aussi le seul moyen de livraison : 
la table de livraison attend une recette dans une assiette.
- Les `CookingContainerItem` : le seul existant dans cette version du jeu est la 
poêle. Elle ne peut accueillir qu'un steak découpé, et produira un steak cuit si 
déposée sur une plaque de cuisson.

De la même façon qu'il est possible de déposer un aliment porté par le personnage 
dans un conteneur, il est aussi possible de transvaser un aliment ou une recette d'un 
conteneur vers un autre, sachant que le sens privilégié est `CookingContainerItem` => `ContainerItem` 
(i.e. une assiette).

#### Extincteur

L'extincteur est un objet qui peut être activé lorsqu'il est porté par le joueur. Il 
permet d'éteindre une table en feu.

### Logique de jeu 

Pour la logique du jeu, nous nous éloignons un peu du paradigme de l'orienté-objet 
car ces objets sont conçus pour encadrer le gameplay du jeu. Deux éléments principaux 
ont été implementés:
- le joueur 
- la gestion des niveaux 

#### Le joueur

Le joueur est géré grâce au `Player Controller` qui reçoit les commandes de direction pour 
gérer les déplacements du joueur et mettre à jour sa position à l'écran. Il reçoit les
commandes des touches C et V pour déclencher les actions correspondantes décrites dans 
le `Action Handler`. C'est cet objet qui garde trace des interactions du joueur (fait-il 
face à une table? porte-il un objet?). La logique développée est ensuite assez simple : 
- pour la touche C : si il porte un objet, il essaie de le poser sur la table (La méthode PutDown 
doit ensuite elle distinguer les cas et éxécuter la bonne action correspondante), 
sinon il essaie de prendre un objet sur la table.
- pour la touche V : si il tient un objet utilisable (autrement dit l'extincteur), il 
l'utilise, sinon il essaie d'utiliser la table.
Les autres méthodes visent simplement à différencier un appui ponctuel d'un appui prolongé, 
pour faire fonctionner les actions asynchrones correctement.

Ne reste plus qu'à pouvoir détecter les objets autour du joueur, ce qui est réalisé grâce à
l'`Item Detector`. En utilisant le moteur physique de Unity on peut détecter des 
"collisions" entre objets, ce qui permet de détecter les objets devant le joueur et donc 
de permettre au joueur d'intéragir avec les tables devant lui.

#### La gestion du niveau 

Toute la gestion des niveaux se fait à l'aide des "Controller" implémentés dans le dossier 
`Controllers`:
- le `gameController` est le grand architecte d'un niveau : il lance la création la scène 
correspondant, instancie le joueur, fait le lien entre nos objets et les autre controllers 
et finalement termine le niveau lorsque toute les recettes ont soit été traitées, 
ou qu'elles n'ont pas été traitées dans le temps imparti.
- le `recipeController` gère les commandes des clients. Il génère des recettes à un 
intervalle de temps aléatoire compris entre deux bornes (tout en prenant en compte un 
nombre maximal de commandes simultanées) et se charge de traiter les assiettes livrées par
le joueur pour les comparer aux recettes attendues. Ce controller était l'un des plus 
challengeants du fait de la gestion particulière des Threads dans Unity comme détaillé 
ci-dessous.
- le `scoreController` prend en charge tous les traitements du score du joueur autour 
du niveau.
- le `cameraController`, un peu à part, controle le placement de la caméra de manière 
à suivre les mouvements du joueur.

Ne nous restait plus qu'à gérer l'enchainement entre les différents niveaux et le menu, 
ce que nous avons pu réaliser avec les fonctions Unity et au moyen des fichiers du dossier 
`Menu` nous permettant de gérer ces transitions.


### Évènements asynchrones
La gestion des processus asynchrones se fait de deux manières dans notre projet Unity. 
La première utilise des Threads, et l'autre utilise la manière d'Unity de gérer l'asynchrone:
les Coroutines.

Ces deux méthodes sont utilisées dans des contextes différents, à savoir la gestion des
timers de recettes pour les Threads, et la gestion de la préparation des ingrédients
pour les Coroutines.

#### Les processus asynchrones dans des Threads
La gestion des Threads dans Unity est un peu particulière. La gestion des GameObjects de Unity
et les mises à jour visuelles de ces derniers, se fait dans le `main Thread`, et uniquement dans le `main Thread`.
Ainsi, si nous créons un nouveau Thread, ce dernier n'est pas autorisé à mettre à jour
les propriétés visuelles des GameObjects, ou même d'en créer des nouveaux.

Pour gérer les timers des attentes clients, nous utilisons des Thread dans un Sémaphore. 
Le Sémaphore nous permet de gérer le nombre de timers qu'on a en simultané, pour avoir un 
max de commandes en attente, et ne pas trop en demander au joueur.
Ainsi, pour pouvoir mettre à jour visuellement ces timers, nous devons faire les mises 
à jour graphiques depuis le `main Thread`. Nous utilisons un système d'`Actions`
pour cela, car il nous permet de faire communiquer les Threads de timer avec le Thread Principal. 
De même, la création de ces timers toutes les `x` secondes se fait également via des Thread,
mais un Thread n'est pas autorisé à créer de GameObject. On déclenche ainsi une action 
`CreateTimerEvent` pour déclencher la création d'un nouveau Timer depuis le `main Thread`.

#### Les processus asynchrones dans des Coroutines
Les Coroutines sont plus naturelles à utiliser dans Unity car leur fonctionnement repose
sur la gestion de l'asynchrone dans Unity, qui est très similaire à celui de NodeJS, basé
sur une boucle d'événements. Les Coroutines fonctionnent de la même manière que l'`async-await`
en NodeJS, et est alors réalisé entièrement sur le Thread principal.

Nous avons découvert l'existence des Coroutines un peu tard dans le projet, et c'est en partie 
pour cela que nous ne les avons pas utilisées pour le système de timers de recettes.
De plus, l'utilisation d'un Sémaphore avec ce type d'objets n'est pas possible, et nous 
n'avons pas trouvé d'équivalent. Nous avons ainsi ainsi gardé toute la partie en Threads
pour la gestion des récettes.

Nous utilisons ce procédé pour gérer les Timers de cuisson, et l'avancée de la découpe
d'un ingrédient.

### Contraintes de développement sur Unity 
Le fait de travailler sous Unity nous a permis d'avoir une interface graphique 3D assez
rapidement, mais nous a également beaucoup contraint.

Comme vu dans la section *[Evenements asynchrones](#Évènements asynchrones)*, la gestion 
des Threads sous Unity ne permet pas d'interagir avec le visuel d'un GameObject, ce qui 
nous a amenés à développer un moyen de déclencher des actions dans le `main Thread` à partir
d'un autre thread. Mais la gestion des objets dans Unity a également contraint la manière 
dont nous avons pu créer nos classes.

En effet, les GameObject dans Unity ne peuvent pas être aussi encapsulés qu'on le voudrait,
car pour y attacher des propriétés visuelles, on est forcé de définir ces attributs 
comme `public`. 

De plus, il n'est pas possible de changer totalement le visuel (ou `Prefab`) d'un GameObject 
après qu'il ait été instancié. Par exemple, pour passer d'un steak découpé à un steak cuit,
le changement d'un attribut d'état de `découpé` à `cuit`, ne suffit pas par exemple. Nous
sommes obligés de détruire l'objet, et d'en créer un nouveau à la même position, avec un
autre `Prefab`. Si cet objet était lié à un autre (par exemple s'il était posé sur une table),
il faut alors suite à la recréation de l'objet, renseigner le lien avec la table à nouveau, 
nous forçant à faire remonter les objets dans les méthodes, complexifiant beaucoup la logique
du code.
Cette problématique a été la plus impactante des contraintes rencontrées sur Unity, celle qui
a littéralement influencé l'architecture de notre solution.

### Gestion des exceptions
Dans le cadre du jeu, nous avons un certain nombre d'actions interdites que le joueur ne peut performer. 
Dans ce contexte, nous avons créé une exception particulière de manière à pouvoir générer un son lorsque l'on 
"catch" une exception de ce type pour prévenir le joueur que son action ne s'éxécute pas pour une raison bien précise. 
On catch donc les erreurs autour des deux actions possibles des joueurs (```Action C``` et ```Action V```) de manière à 
être toute erreur de manière certaine, même en cas de comportements imprévus du joueur.


## Perspectives du projet

A l'issue du temps imparti pour le projet nous avons réussi à atteindre un stade assez avancé 
de MVP qui satisfait les objectifs que nous nous étions fixés : un jeu solo tournant 
en local et reprenant les principaux mécanismes de *Overcooked*. Nous sommes ainsi 
capables de demander, préparer et livrer plusieurs types de recettes nécessitant différentes 
formes de préparation (assemblage d'ingrédient, découpe, cuisson, etc.) tout en ajoutant 
un peu de difficulté, avec la possiblité pour la cuisson de prendre feu si le timing 
n'est pas respecté (s'en suivant la propagation du feu, la crémation d'ingrédients ou 
de recette et, si tout se passe bien, l'extinction du ou des feux).

Toutefois comme tout projet, il est un certain nombre d'éléments que nous avions planifiés 
en bonus mais que nous n'avons pu réaliser par manque de temps et une faible priorisation 
de ces tâches : 
- ajout de recette supplémentaire : de manière très simple nous aurions souhaité varier 
davantage les recettes en proposant des contenus différents (salade avec des tomates, 
steak frites, soupes, etc.) de manière à varier l'expérience et augmenter la difficulté. 
Ayant essayé d'adopter une structure open-close, l'inclusion de nouveaux ingrédients et de 
nouvelles recettes pouvaient se faire sans trop de difficultés en terme de script : 
ils nous aurait suffit de rajouter des scripts pour les ingrédients en leur faisant implémenter 
les interfaces `IChoppable` et `ICookable` suivant les cas ainsi que de les inclure 
dans le `RecipeAssembler` (et les enum `Ingredients` et `Recipes`). La principale 
contrainte qui nous a fait la déprioriser résidait dans le fait de trouver et 
mettre en forme des Prefabs (c'est-à-dire les objets de jeu physiques traités par le 
moteur de Unity) ce qui prend un temps conséquent.
- ajout de la logique de vaisselle : une des features du jeu original ajoutant un 
peu de difficulté est le fait qu'il n'y a qu'un nombre limité d'assiettes en jeu 
(ce que nous avons reproduit) et que celles-ci ne peuvent pas juste être récupérées à 
une source mais réapparaissent sales après la livraison de leur contenu. 
Le joueur doit ensuite les laver si il veut les réutiliser. La logique de saleté et 
de vaisselle avait déjà quasiment été implémentée lors de l'écriture de la classe 
`ContainerItem` en créant un nouveau type de `TriggerableActionTable` : les éviers `Sink` 
pour nettoyer les assiettes. Toutefois l'inclusion de la feature complète (regénération 
d'assiette sale un certain temps après livraison tout en ayant un nombre limité 
d'assiette) représentait une tâche longue sans apporter une immense valeur ajoutée 
en termes de complexité (une logique assez similaire à la génération de timers) ou 
de gameplay. Pour toutes ces raisons, la feature de vaiselle n'est pas présente 
dans ce projet mais serait vite incluse dans d'éventuels développement ultérieurs du 
projet.
- refonte de la logique de `RecipeAssembler` : pilier fonctionel permettant d'assembler
les ingrédients pour former des recettes, cette classe constitue néansmoins le point 
faible de notre architecture sur le plan Open-close. Avec la plupart de sa logique 
passant à travers d'immenses `switch` devant traduire toutes les combinaisons possibles 
d'ingrédients, il constitue la plus grosse barrière à l'ajout de nouveau ingrédients. Dans 
l'optique de poursuivre le projet, une refonte de cette classe serait donc une priorité. 

Enfin à plus long terme, la poursuite de ce projet passerait par l'ajout d'un mode 
multi-joueur d'abord en local puis en ligne de manière à permettre des situations de 
coopération qui font vraiment le coeur du jeu.

## Authors

* **Arthur Goutallier** - [GitLab gouta](https://gitlab.com/Gouta)
* **Nicolas Topuz** - [GitLab nico_ajax](https://gitlab.com/nico_ajax)
* **Pierre Averous** - [GitLab piaverous](https://gitlab.com/piaverous)

## Remerciements
Merci à tous nos béta-testeurs, qui nous ont permis d'améliorer l'UX du jeu:
- Hugo Ravel
- Eleonore Bouvier
- Riad Ghorra
- Marjorie Saison
- Antoine Marchais
- Donatien Criaud
- Laura Hennequin
- Guillaume Egee
- Julen Dixneuf
- Quentin Churet
- Mr. L'Encadrant
- Nous-mêmes


