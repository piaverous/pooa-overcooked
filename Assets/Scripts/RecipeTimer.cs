﻿using System;
using System.Threading;
using Items.Recipes;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

/// <summary>
/// This class implements the recipe timers
/// </summary>
public class RecipeTimer : MonoBehaviour
{
    // The loading circle
    public Image loader;
    // The recipe icon
    public Image icon;
    
    // The action called upon ending. It takes a boolean as param, to know whether the timer ended
    // after the recipe was delivered, or if it has timed out.
    public event Action<bool> End;
    // The id of the recipe timer
    public int id;
    
    private bool _remove;
    private int _seconds;
    private int _totalSeconds;
    private float? _fillAmount;
    private Thread _timer;
    private static int _idCounter = 0;
    private Recipe _expectedFoodType;

    /// <summary>
    /// A function used to instatiate a recipe timer
    /// </summary>
    /// <param name="prefab">The prefab of the recipe timer</param>
    /// <param name="recipeType">The recipe</param>
    /// <param name="seconds">The duration of the timer</param>
    /// <param name="offset">An offset so that a new recipe timer won't hide or be hidden by a previous one</param>
    /// <returns></returns>
    public static RecipeTimer Create(Object prefab, Recipe recipeType, int seconds, Vector3? offset)
    {
        // We instantiate the prefab
        GameObject newObject = (GameObject) Instantiate(prefab);
        RecipeTimer newRecipeTimer = newObject.GetComponent<RecipeTimer>();
        // And add the recipe icon
        addIcon(newRecipeTimer, recipeType);

        // We move it to the right of the lately created timer, if it exists
        if (offset != null)
            newRecipeTimer.MoveRight(offset.Value);
        // We set the timer properties
        newRecipeTimer._seconds = seconds;
        newRecipeTimer.id = _idCounter;
        newRecipeTimer._expectedFoodType = recipeType;
        // We increment the timer ids counter
        _idCounter++;
        return newRecipeTimer;
    }

    /// <summary>
    /// A function used to add the icon of the recipe below the timer
    /// </summary>
    /// <param name="recipeTimer">The recipe timer under which the icon will be placed</param>
    /// <param name="recipeType">The recipe</param>
    public static void addIcon(RecipeTimer recipeTimer, Recipe recipeType)
    {
        // Depending on the recipe, we load the corresponding sprite from the resources folder
        switch (recipeType)
        {
            case Recipe.FullBun:
                recipeTimer.icon.sprite = Resources.Load<Sprite>("Icons/burger_steak_tomato_cheese_icon");
                break;
            case Recipe.BunWithSteak:
                recipeTimer.icon.sprite = Resources.Load<Sprite>("Icons/burger_steak_icon");
                break;
            case Recipe.BunWithSteakAndCheese:
                recipeTimer.icon.sprite = Resources.Load<Sprite>("Icons/burger_steak_cheese_icon");
                break;
            case Recipe.BunWithTomatoAndSteak:
                recipeTimer.icon.sprite = Resources.Load<Sprite>("Icons/burger_steak_tomato_icon");
                break;
            default:
                recipeTimer.icon.sprite = Resources.Load<Sprite>("Icons/unknown_icon");
                break;
        }   
    }

    /// <summary>
    /// A function used to start the timer
    /// </summary>
    public void StartTimer ()
    {
        End += (bool timedOut) => Destroy(transform.gameObject);
        _totalSeconds = _seconds;
        // We create a thread for each timer
        _timer = new Thread(() =>
        {
            while (_seconds > 0)
            {
                Thread.Sleep(1000);
                _seconds -= 1;
                _fillAmount = (float) _seconds/_totalSeconds;
            }
            _remove = true;
        });
        _timer.Start();
    }

    /// <summary>
    /// A function used to move a timer to the right
    /// </summary>
    /// <remarks>
    /// Used when creating a new timer, to put it to the right of the other existing timers
    /// </remarks>
    /// <param name="offset">The offset move distance</param>
    public void MoveRight(Vector3 offset)
    {
        foreach (Transform child in transform)
        {
            child.transform.position += offset;
        }
    }

    /// <summary>
    /// A function used move a timer to the left
    /// </summary>
    /// <param name="offset">The offset move distance</param>
    public void MoveLeft(Vector3 offset)
    {
        foreach (Transform child in transform)
        {
            child.transform.position -= offset;
        }
    }

    // Every frame, we update the loader image according to the progression of the timer
    void Update()
    {
        if (_fillAmount != null)
        {
            loader.fillAmount = _fillAmount.Value;
            _fillAmount = null;
        }

        if (_remove && End != null)
            End(true);
    }

    /// <summary>
    /// A function used to end the timer. 
    /// </summary>
    /// <returns>The score obtained for the completion of this timer</returns>
    public int Serve()
    {
        if (End != null)
            End(false);
        int numberOfIngredientsRequired = RecipeAssembler.Disassemble(_expectedFoodType).Count;
        return 10*numberOfIngredientsRequired + _seconds;
    }

    public Recipe GetRecipeType()
    {
        return _expectedFoodType;
    }
}