﻿using Constants;
using UnityEngine;

/// <summary>
/// This class implements the logic of the interaction between the user and the game
/// </summary>
/// <remarks>
/// The player controller will be looking for user's inputs on the keyboard, and will call the action handler's associated actions 
/// </remarks>
public class PlayerController : MonoBehaviour
{
    // The player's movement speed
    public float speed;
    // The animator, that controls the character's animation system in Unity
    public Animator animator;
    // The character's rigidbody, used to simulate physics in Unity
    private Rigidbody _rb;
    // The vector storing the inputs of the user
    private Vector3 _inputs = Vector3.zero;
    // This property links the speed to the animations in game
    private static readonly int MoveSpeed = Animator.StringToHash("MoveSpeed");
    // The Action Handler associated to the player
    ActionHandler action;

    // Before the first frame update, we link the reference to the character's rigid body and the action handler
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        action = GetComponent<ActionHandler>();
    }

    // Every frame, the player controller is looking for user's inputs on the keyboard
    private void Update()
    {
        _inputs = Vector3.zero;
        // Storing the horizontal arrow inputs
        _inputs.x = Input.GetAxis("Horizontal");
        // Storing the vertical arrow inputs
        _inputs.z = Input.GetAxis("Vertical");
        
        // While the user press the left shift key, the character will run instead of walking
        if (Input.GetKey(KeyCode.LeftShift))
        {
            _inputs.x *= GameConstants.RUN_SCALE;
            _inputs.z *= GameConstants.RUN_SCALE;
        }
        
        // If any arrow key has been pressed, we can move the character
        if (_inputs != Vector3.zero)
        {
            transform.forward = _inputs;
            animator.SetFloat(MoveSpeed, _inputs.magnitude);
        }

        // If the C key is pressed, calls the first action in the action handler
        if (Input.GetKey(KeyCode.C)) 
            action.ActionC();

        // If the V key is pressed, calls the second action in the action handler
        if (Input.GetKey(KeyCode.V))
            action.ActionV();

        // Used to stop the second action
        else
            action.ActionVUp();
    }

    // We move the rigidbody according to user inputs
    void FixedUpdate()
    {
        _rb.MovePosition(_rb.position + Time.fixedDeltaTime * speed * _inputs);
    }
}
