﻿using UnityEngine;
using System.Threading;
using Constants;

/// <summary>
/// This class implements all the actions than can be performed by the player.
/// </summary>
public class ActionHandler : MonoBehaviour
{
    // Focus related properties, set with ItemDetector
    public GameObject tableInFocus = null; // A reference to the Table in focus

    // A reference to the Item picked up by the player
    public GameObject itemPickedUp = null;

    // A reference to the Player GameObject
    private GameObject _player;
    // A reference to the Carry Slot Gameobject, the volume where the picked up item will be stored in game
    private GameObject _carrier;
    // A boolean, used to add delay between action thanks to threads, preventing bugs
    private bool _throttling;

    // The audio source used whenever the player performs an unauthorized action
    private AudioSource unauthorizedAction;

    // We link the reference to the actual Audio Source of unauthorized actions
    private void Awake()
    {
        unauthorizedAction = this.GetComponent<AudioSource>();
    }

    // Before the first frame update, we link the reference to the player and carrier game objects
    private void Start()
    {
        _player = transform.gameObject;
        _carrier = _player.transform.Find("CarrySlot").gameObject;
    }

    /// <summary>
    /// This function is called when the player performs the first possible action.
    /// </summary>
    /// <remarks>
    /// This action is essentially linked to picking up and putting down items.
    /// </remarks>
    public void ActionC()
    {
        try
        {
            // If there is no table in focus, it's impossible to perform any action. 
            if (tableInFocus == null)
            {
                throw new System.Exception("No possible interaction");
            }
            // We store the table in focus
            Table table = tableInFocus.GetComponent<Table>();

            // If there is no item currently picked up, and the throttling is over
            if (itemPickedUp == null && !_throttling)
            {
                // Trying to pick up the item on table
                Item item = table.PickUp(_carrier);
                itemPickedUp = item.gameObject;
                // Activating the throttling
                ThrottleKeyPressed();
            }

            // If there is an item currently picked up, and the throttling is over
            else if (itemPickedUp != null && !_throttling)
            {
                // Trying to select the picked up item
                Item item = itemPickedUp.GetComponent<Item>();
                // And putting it down on the table
                Item newObject = table.PutDown(item, _carrier);
                itemPickedUp = (newObject != null) ? newObject.gameObject : null;
                // Activating the throttling
                ThrottleKeyPressed();
            }
        }
        catch (System.Exception e)
        {
            // Whenever the user performs an unauthorized action (e.g. trying to pick up an item when there is no item on the table in focus)
            if (e is UnauthorizedActionException)
                // We play an error sound
                unauthorizedAction.Play();
            Debug.LogError(e.Message);
        }
    }

    /// <summary>
    /// This function is called when the player performs the second possible action.
    /// </summary>
    /// <remarks>
    /// This action is essentially linked to actions of using a table or an item (e.g. chopping a food item, using the extinguisher)
    /// </remarks>
    public void ActionV()
    {
        try
        {
            // If there is no table in focus, it's impossible to perform any action.
            // We try to store the table in focus.
            Table table = tableInFocus.GetComponent<Table>();

            // If there is an item currently picked up
            if (itemPickedUp != null)
            {
                Item item = itemPickedUp.GetComponent<Item>();
                // If the item is an extinguisher (only item that can be used picked up), the throttling is over, and there is a table in focus
                if (item is Extinguisher && !_throttling && !(table is null))
                {
                    // The extinguisher is used on the table in focus
                    ((Extinguisher)item).Extinguish(table);
                    // Activating the throttling
                    ThrottleKeyPressed();
                }
                else
                {
                    throw new UnauthorizedActionException("You can't use this object or use this table while holding an object");
                }
            }
            // If the table in focus is a triggerable table and the throttling is over
            else if (table is TriggerableActionTable && !_throttling)
            {
                // The table is used
                ((TriggerableActionTable)table).Use();
                // Activating the throttling
                ThrottleKeyPressed();
            }
        }
        catch (System.Exception e)
        {
            // Whenever the user performs an unauthorized action
            if (e is UnauthorizedActionException)
                // We play an error sound
                unauthorizedAction.Play();
            Debug.LogError(e.Message);
        }

    }
    
    /// <summary>
    /// This function is called when the player stopped performing the second action.
    /// </summary>
    public void ActionVUp()
    {
        if (!tableInFocus)
            return;
        
        Table table = tableInFocus.GetComponent<Table>();
        // If the table in focus was indeed a triggerable action table
        if (table is TriggerableActionTable)
        {
            // The table stops being used
            ((TriggerableActionTable)table).StopUse();
        }
    }

    /// <summary>
    /// This function is called to activate the throttling.
    /// </summary>
    /// <remarks>
    /// Threads are being used to trigger the throttling. They ensure there won't be any bug because some actions were performed too quickly.
    /// </remarks>
    private void ThrottleKeyPressed()
    {
        // The throttling boolean is set on
        _throttling = !_throttling;
        // We create a new thread
        new Thread(() =>
        {
            // The thread sleeps for a predetermined duration of time
            Thread.Sleep(TimeConstants.KEYPRESS_THROTTLE_TIME);
            // When sleep is over, the throttling boolean is set off, so that a new action can be performed
            _throttling = !_throttling;
        }).Start();
    }
}
