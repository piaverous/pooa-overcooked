﻿using UnityEngine;

/// <summary>
/// This class implements the logic of item detection by the player.
/// </summary>
/// <remarks>
/// This class uses Unity's Physics.Raycast, which casts a ray against all colliders in the scene.
/// </remarks>
public class ItemDetector : MonoBehaviour
{
    // The Action Handler associated to the player
    private ActionHandler action;

    // We link the reference to the actual Action Handler
    private void Awake()
    {
        action = GetComponent<ActionHandler>();
    }

    // Every frame, a ray is cast in front of the player, looking for items
    void Update()
    {
        // The raycast
        RaycastHit hit;
        // The direction and length of the raycast
        Vector3 direction = transform.up * 2f ;
        // The origin point of the raycast
        Vector3 origin = transform.position + transform.forward * 0.75f;
        // For debug purposes, The line will be drawn in the Scene view of the editor. If gizmo drawing is enabled in the game view, the line will also be drawn there.
        Debug.DrawRay(origin, direction, Color.green);

        // If the line collides with an item
        if (Physics.Raycast(origin, direction, out hit, 2.0f))
        {
            // If the item implements the Table class
            if (hit.collider.gameObject.GetComponent<Table>() != null)
            {
                // Pass the Table that is now in focus to the Action Handler
                action.tableInFocus = hit.collider.gameObject;
                // Highlights the Table
                action.tableInFocus.GetComponent<Table>().Highlight(true);
            }
        }

        // If the line doesn't collide with an item anymore
        else
        {
            // If a Table was in focus before
            if (action.tableInFocus != null)
            {
                // Unhighlight it
                action.tableInFocus.GetComponent<Table>().Highlight(false);
                // Remove it from the Action Handler
                action.tableInFocus = null;
            }
        }
    }
}