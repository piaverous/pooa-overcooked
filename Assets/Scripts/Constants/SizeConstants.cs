namespace Constants
{
     /// <summary>
     /// A static class defining physical measures of assets 
     /// </summary>
    public static class SizeConstants
    {
        /// <value> The height of a table </value>
        public static readonly float TABLE_HEIGHT = 0.7f;

        /// <value> The height of a timer </value> 
        public static readonly float TIMER_HEIGHT = 0.2f;

        /// <value> The height of the player </value> 
        public static readonly int PLAYER_HEIGHT = 2;
    }
}