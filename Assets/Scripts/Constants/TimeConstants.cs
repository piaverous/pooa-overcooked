namespace Constants
{
    /// <summary>
    /// A static class defining all time and duration constants 
    /// </summary>
    public static class TimeConstants
    {
        // The interval for the timers duration
        public static readonly int TIMER_MIN_DURATION = 90;
        public static readonly int TIMER_MAX_DURATION = 120;

        // The interval for the amount of time between each timer/recipe generation 
        public static readonly int TIMER_GENERATION_CYCLE_MIN = 15;
        public static readonly int TIMER_GENERATION_CYCLE_MAX = 30;

        // The maximal amount of recipe that are being requested at the same time 
        public static readonly int MAX_CONCURRENT_RECIPES = 4;

        // The time constant of the granularity of animations and the detection of keypress throttling
        public static readonly int ANIMATION_TIME_GRANULARITY = 25;
        public static readonly int KEYPRESS_THROTTLE_TIME = 250;

        // The time for a steak to cook and to burn
        public static readonly float COOKING_DURATION = 7;
        public static readonly float CHOPPING_DURATION = 1.5f;
        public static readonly float BURNING_DURATION = 4;

        // The time interval before the fire tries to propagate from a table to another
        public static readonly int TIME_BETWEEN_FIRE_PROPAGATION = 5;
    }
}