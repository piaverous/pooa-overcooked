using System.Collections.Generic;
using Items.Recipes;

namespace Constants
{
    /// <summary>
    /// A static class to define the main constant and parameters for the game experience
    /// </summary>
    public static class GameConstants
    {

        /// <value> The number of recipes per level </value> 
        public static readonly int RECIPES_TO_END = 5;

        ///  <value> A dictionnary defining the list of possible recipes for each level </value>
        public static readonly Dictionary<int, List<Recipe>> POSSIBLE_END_RECIPES_BY_LEVEL =
            new Dictionary<int, List<Recipe>> { 
                {
                    1, new List<Recipe> {
                        Recipe.BunWithSteakAndCheese
                    }
                }, {
                    2, new List<Recipe> {
                        Recipe.BunWithSteakAndCheese,
                        Recipe.BunWithTomatoAndSteak,
                        Recipe.FullBun
                    }
                }, {
                    3, new List<Recipe> {
                        Recipe.FullBun
                    }
                }
            };

        /// <value> The maximal number of plate at the same time in a level </value>
        public static readonly int PLATE_NUMBER_MAX = 2;

        /// <value> The difference between walking and running </value>
        public static readonly float RUN_SCALE = 2.5f;

    }
}