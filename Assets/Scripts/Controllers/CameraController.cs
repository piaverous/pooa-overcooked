﻿using UnityEngine;

namespace Controllers
{
    /// <summary>
    /// The controller for the gameplay Camera object.
    /// The camera moves along with the player, for a third-person view
    /// </summary>
    public class CameraController : MonoBehaviour
    {
        private GameObject _player;
        private Vector3 _offset;

        /// <summary>
        /// This moves the camera around, following the player's movements. 
        /// </summary>
        void LateUpdate()
        {
            if(_player)
                transform.position = _player.transform.position + _offset;
        }

        /// <summary>
        /// This function gives the camera controller a reference to the current
        /// player. This allows it to move around following the player.
        /// </summary>
        /// <param name="player">The gameobject referencing the player character</param>
        /// <param name="initialPosition">The Player's initial position</param>
        public void SetPlayer(GameObject player, Vector3 initialPosition)
        {
            _player = player;
            transform.position = transform.position + initialPosition;
            _offset = transform.position - _player.transform.position;
        }
    }
}
