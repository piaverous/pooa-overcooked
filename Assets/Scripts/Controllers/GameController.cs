using UnityEngine;

namespace Controllers
{
    /// <summary>
    /// This class will controll the game from a high level.
    /// It will do things like Starting a level, binding objects together,
    /// or say a level is over.
    /// </summary>
    public class GameController : MonoBehaviour
    {
        // References to other general objects that will either be 
        // instantiated by the GameController, or linked to it
        public RecipeController recipeController;   // Reference to the RecipeController script.
        public GameObject playerPrefab;             // Reference to the prefab the players will control.
        public EndGameScreen endGamePrefab;         // Reference to the prefab the end game screen.
        public CameraController cameraController;   // Reference to the CameraController to bind it to the Player.
        public ScoreController scoreController;     // Reference to the ScoreController script.
        
        // Given knowledge of the current level
        public int currentLevel;
       
        // Initial position for the player on this level, set in Unity for each level
        public Vector3 initialPlayerPosition;

        // Store instances of the Score- and RecipeController
        private ScoreController _scoreControllerInstance;
        private RecipeController _recipeControllerInstance;
        
        private void Start()
        {
            // If not in the menu, i.e. level is strictly positive, then start the game.
            if (currentLevel > 0)
                StartLevel(false);
            
            // If in the tutorial, start accordingly
            if(currentLevel < 0)
                StartLevel(true);
        }
        
        
        /// <summary>
        /// Spawns the player at the position we wish to place him
        /// </summary>
        /// <param name="position">The position where the player should spawn</param>
        private void SpawnPlayer(Vector3 position)
        {
            GameObject player = Instantiate(playerPrefab, position, new Quaternion(0, 180, 0, 0));
            cameraController.SetPlayer(player, position);
        }

        /// <summary>
        /// Starts the level by spawning the player, instantiating the recipe and score
        /// controllers, setting them up, and officially beginning the game.
        /// </summary>
        private void StartLevel(bool inTutorial)
        {
            SpawnPlayer(initialPlayerPosition);
            _recipeControllerInstance = Instantiate(recipeController);
            _recipeControllerInstance.SetCurrentLevel(currentLevel);

            if (!inTutorial)
            {
                _scoreControllerInstance = Instantiate(scoreController);
            
                _recipeControllerInstance.SetGameController(this);
                _recipeControllerInstance.SetScoreController(_scoreControllerInstance);
            
                // Everything is set up, let's start the level.
                _recipeControllerInstance.BeginLevel();
            }
        }

        /// <summary>
        /// Called whenever the game is over.
        /// Will bring the Level ending screen forward, displaying the player's
        /// score for this level.
        /// </summary>
        public void EndLevel()
        {
            EndGameScreen endScreen = Instantiate(endGamePrefab);
            endScreen.SetScore(_scoreControllerInstance.GetScore());
            endScreen.SetCurrentLevel(currentLevel);
        }

        /// <summary>
        /// Allows certain objects to access the current instance of the recipeController.
        /// </summary>
        /// <returns>The current instance of the RecipeController</returns>
        public RecipeController GetRecipeController()
        {
            return _recipeControllerInstance;
        }
    }
}