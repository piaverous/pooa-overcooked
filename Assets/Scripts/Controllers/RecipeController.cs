﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using Random = System.Random;
using Constants;
using Items.Recipes;

namespace Controllers
{
    /// <summary>
    /// The RecipeController is used to control the flow of recipes that are
    /// requested by clients. It knows how much time is left for which recipe,
    /// when to add a new timer and when to end the game.
    /// </summary>
    public class RecipeController : MonoBehaviour
    {
        public GameObject timerPrefab;
        
        private ScoreController _scoreController;
        private GameController _gameController;
        private Semaphore _pool;

        private uint _timerCount;
        private int _currentLevel;
        private List<RecipeTimer> _existingTimers = new List<RecipeTimer>();
        private readonly Random _rnd = new Random();

        // An event that is called whenever a new Recipe Timer should be created
        private event Action CreateTimerEvent;

        /// <summary>
        /// Starts the level, by initializing the empty Semaphore used to store
        /// the Timer threads
        /// </summary>
        public void BeginLevel()
        {
            _pool = new Semaphore(
                TimeConstants.MAX_CONCURRENT_RECIPES, 
                TimeConstants.MAX_CONCURRENT_RECIPES
            );
            // Add an action to the event
            CreateTimerEvent += () => NextTimer();
        }

        /// <summary>
        /// After each frame update, 
        /// </summary>
        private void LateUpdate()
        {
            if (CreateTimerEvent != null && _timerCount < GameConstants.RECIPES_TO_END)
            {
                _timerCount++;
                CreateTimerEvent();
                CreateTimerEvent = null;
            }
        }

        /// <summary>
        /// This method creates a new timer with a given duration in seconds
        /// </summary>
        /// <param name="duration">The duration of the timer</param>
        void AddTimer(int duration)
        {
            // Timers are displayed in a list on the top of the screen
            // From the side of the screen, each timer is offset by the width 
            // given for a single timer.
            Vector3? totalOffset = null;
            if (_existingTimers.Count > 0)
                totalOffset = _existingTimers.Count * new Vector3(Screen.width/12, 0, 0);

            // Pick a random recipe in the recipes for this given level
            int indexOfRecipeType = _rnd.Next(GameConstants.POSSIBLE_END_RECIPES_BY_LEVEL[_currentLevel].Count);
            Recipe recipeType = GameConstants.POSSIBLE_END_RECIPES_BY_LEVEL[_currentLevel][indexOfRecipeType];
            
            // Create the timer, and assign it an ending callback
            RecipeTimer newRecipeTimer = RecipeTimer.Create(timerPrefab, recipeType, duration, totalOffset);
            newRecipeTimer.StartTimer();
            newRecipeTimer.End += (bool timedOut) => End(newRecipeTimer, timedOut);
            _existingTimers.Add(newRecipeTimer);
        }

        /// <summary>
        /// End a given timer
        /// </summary>
        /// <param name="recipeTimer">The timer to end</param>
        /// <param name="timedOut">
        /// A boolean, letting you know if the timer ended
        /// because it timed out or not (recipe delivery)
        /// </param>
        public void End(RecipeTimer recipeTimer, bool timedOut)
        {
            // Remove the timer that is finished
            int index = _existingTimers.FindIndex(t => t.id == recipeTimer.id);
            _existingTimers.RemoveAt(index);
        
            // Move all timers located to the right of the deleted one, to the left
            for (int i=index; i<_existingTimers.Count; i++)
            {
                _existingTimers[i].MoveLeft(new Vector3(Screen.width/12, 0, 0));
            }
            _pool.Release(); // Free a spot in the Semaphore

            // If the timer timed out, lose 20 points. Eventually end the game, if it was the last timer
            if (timedOut)
            {
                _scoreController.ScorePoints(-20);
                if( _timerCount == GameConstants.RECIPES_TO_END && _existingTimers.Count == 0) 
                    _gameController.EndLevel();
            }
        }

        /// <summary>
        /// Create the next timer that should be created
        /// </summary>
        private void NextTimer()
        {
            int timerDuration = _rnd.Next(TimeConstants.TIMER_MIN_DURATION, TimeConstants.TIMER_MAX_DURATION);
            AddTimer(timerDuration);

            // Planify the creation of the next timer
            int timeBeforeNextTimer = _rnd.Next(TimeConstants.TIMER_GENERATION_CYCLE_MIN, TimeConstants.TIMER_GENERATION_CYCLE_MAX);
            new Thread(() =>
            {
                // After waiting for a couple seconds and for a spot in the Semaphore to be free, create the next timer.
                // N.B. here we do not call NextTimer immediately, because Unity does not allow a child Thread
                // to create GameObjects by itself. Therefore we add an action to the CreateTimerEvent, which 
                // executes it in the main Thread, checking on every frame update if it should do anything or not.
                Thread.Sleep(timeBeforeNextTimer*1000);
                _pool.WaitOne();
                CreateTimerEvent += () =>  NextTimer();
            }).Start();
        }

        /// <summary>
        /// Method used to see all recipes that clients are waiting for.
        /// </summary>
        /// <returns>An IEnumerable, containing all recipes awaited</returns>
        private IEnumerable<Recipe> GetCurrentRecipeTypes()
        {
            return _existingTimers.Select(timer => timer.GetRecipeType());
        }

        /// <summary>
        /// Processes the delivery of an Item.
        /// </summary>
        /// <param name="item">The item to be delivered</param>
        /// <remarks>The item is here a ContainerItem, because food has to be delivered in a container</remarks>
        public void ProcessDelivery(ContainerItem item)
        {
            if (item.content == null || _currentLevel < 0) // Do nothing if delivering an empty plate or in tutorial
                return;
            
            IEnumerable<Recipe> currentRecipeTypes = GetCurrentRecipeTypes();
            if (currentRecipeTypes.Contains(item.content.Value))
            {
                RecipeTimer recipeTimer = _existingTimers.Find(timer => timer.GetRecipeType() == item.content.Value);
                int points = recipeTimer.Serve();
                _scoreController.ScorePoints(points);
            }
            else _scoreController.ScorePoints(-30); // Lose 30 points for delivering something unexpected
            
            if(_timerCount == GameConstants.RECIPES_TO_END && _existingTimers.Count == 0)
                _gameController.EndLevel();
        }

        /// <summary>
        /// Gives the recipeController a reference to the scoreController.
        /// </summary>
        /// <param name="scoreController">The score controller used in this level</param>
        public void SetScoreController(ScoreController scoreController)
        {
            _scoreController = scoreController;
        }
        
        /// <summary>
        /// Gives the recipeController a reference to the gameController.
        /// </summary>
        /// <param name="gameController">The game controller for this level</param>
        public void SetGameController(GameController gameController)
        {
            _gameController = gameController;
        }
        
        /// <summary>
        /// Lets the recipeController know what the current level is
        /// </summary>
        /// <param name="currentLevel">The number of the current level</param>
        public void SetCurrentLevel(int currentLevel)
        {
            _currentLevel = currentLevel;
        }
    }
}
