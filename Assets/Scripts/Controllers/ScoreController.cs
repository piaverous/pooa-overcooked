using UnityEngine;
using UnityEngine.UI;

namespace Controllers
{
    /// <summary>
    /// This class is used to control the score on a global level
    /// </summary>
    public class ScoreController : MonoBehaviour
    {
        public Text scoreDisplay; // The text object that displays the score in game
        private int _score; // The score

        private void Start()
        {
            // Initially the score is zero
            UpdateScoreDisplay(0);
        }

        /// <summary>
        /// Score points in the game. This updates the stored score, as well as the
        /// text displayed in the top-right corner of the screen
        /// </summary>
        /// <param name="points">The amount of points scored</param>
        public void ScorePoints(int points)
        {
            _score += points;
            UpdateScoreDisplay(_score);
        }
        
        /// <summary>
        /// Updates the score displayed in the top right corner of the screen
        /// </summary>
        /// <param name="score">The score to display</param>
        private void UpdateScoreDisplay(int score)
        {
            scoreDisplay.text = $"Score: {score}";
        }

        /// <summary>
        /// Public getter to know the score
        /// </summary>
        /// <returns>The score as an int</returns>
        public int GetScore()
        {
            return _score;
        } 
    }
}