﻿using Items.Recipes;
/// <summary>
/// A class implementing the Frying Pan object logic.
/// </summary>
/// <remarks>
/// As such it is a pretty simple implementation of the cooking container item with the specific Prefabs and autorization
/// </remarks>
public class FryingPan : CookingContainerItem
{

    /// The prefabs for the pan with a chopped and a cooked steak
    public CookingContainerItem fryingPanChoppedSteak;
    public CookingContainerItem fryingPanCookedSteak;


    /// <summary>
    /// The method defining if a given fooditem can be cooked using the frying pan
    /// </summary>
    /// <param name="foodItem">The food item that we want to know if we can cook here</param>
    /// <returns>The prefab of the frying pan with the unccoked item in it</returns>
    public override CookingContainerItem CanBeCookedHere(FoodItem foodItem)
    {
        // if there is already an ingredient cooking, we cannot add another one
        if (content != null)
            throw new UnauthorizedActionException("Pan is already full !");

        // Else we go through the list of autorized food item for the frying pan returning the corresponding Prefab
        // If none is found, it means the ingredient is not allowed to be cooked in a frying pan and we throw an exception
        switch (foodItem)
        {
            case ChoppedSteak _:
                return fryingPanChoppedSteak;
            default:
                throw new UnauthorizedActionException("This can't be cooked here, dude.");
        }
    }

    /// <summary>
    /// This functions returns the frying pan containing the cooked food item 
    /// </summary>
    /// <param name="cookingContainerItem"></param>
    /// <returns>A bool specifying if the object has been cooked, the cooked prefab and the content of the newly generated prefab</returns>
    public override (bool, CookingContainerItem, Recipe?) GetCookedContainerItem(CookingContainerItem cookingContainerItem)
    {
        // For all possible recipes in the frying pan, we return the prefab with the corresponding cooked content 
        switch (content)
        {
            case Recipe.ChoppedSteak:
                return (true, fryingPanCookedSteak, Recipe.CookedSteak);
            default:
                return (false, this, null);
        }
    }

}
