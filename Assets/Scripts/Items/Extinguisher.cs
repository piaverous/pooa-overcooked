using UnityEngine;

/// <summary>
/// A class implementing the extinguisher object.
/// </summary>
public class Extinguisher : Item
{
    /// <summary>
    /// The audio source playing the sound of the extinguisher
    /// </summary>
    private AudioSource extinguishingSound;

    private void Awake()
    {
        // Upon awakening we link this reference field to the actual audio source
        extinguishingSound = GetComponent<AudioSource>();
    }

    /// <summary>
    /// A function to use the extinguisher to extinguish the fire on a table
    /// </summary>
    /// <param name="table">The table on fire that you want to stop from burning</param>
    public void Extinguish(Table table)
    {
        // We extinguish the fire on the table and play the corresponding sound
        table.Extinguish();
        extinguishingSound.Play();
    }

    /// <summary>
    /// The function defining how the extinguisher should react if he is to get caught in the fire
    /// </summary>
    /// <remarks>
    /// The extinguisher cannot burn in order to make sure the fire can always be put off.
    /// As a result we just throw an exception to interrumpt the table.catchFire method calling it
    /// </remarks>
    public override Item Burn()
    {
        throw new System.Exception("This one can't burn.");
    }

}