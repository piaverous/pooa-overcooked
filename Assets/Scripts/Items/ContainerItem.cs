﻿using System.Collections.Generic;
using UnityEngine;
using Items.Recipes;
using UnityEngine.UI;

/// <summary>
/// The class implementing the container Item which is to say any item able to welcome some food into it.
/// </summary>
/// <remarks>
/// The basic container Item is the plate and is directly implemented using this script. However it is also extended by the cooking container class for items such as frying pans
/// </remarks>
public class ContainerItem : Item
{
    // Assembling functions
    public Recipe? content;
    public RecipeAssembler recipeAssembler;

    // The prefab of the corresponding container empty and burnt
    public ContainerItem emptyPrefab;
    public ContainerItem burntPlatePrefab;

    // The reference to the icon describing the content of the container
    public Image icon;


    /// <summary>
    /// The function to empty a container
    /// </summary>
    /// <param name="carrier">The carrier currently holding the container if there is one, null if not</param>
    /// <returns>The emptied container</returns>
    public ContainerItem Empty(GameObject carrier = null)
    {
        // If there is no content, we simply return the object as the container is already empty 
        if (content == null)
            return this;

        // We destroy the current game object and instantiate a new container in the same space where the former plate 
        Transform formerTransform = gameObject.transform;
        Destroy(gameObject);
        ContainerItem emptyContainer = Instantiate(emptyPrefab, formerTransform.position, formerTransform.rotation);

        // If there is a carrier then the new object is picked up by the carrier
        if (!(carrier is null))
            emptyContainer.GetPickedUp(carrier);

        // Finally we return the new container
        return emptyContainer;
    }

    /// <summary>
    /// A function the icon describing the content of the container over the container item
    /// </summary>
    public void AddIcon()
    {
        // We set the color for the icons
        var tempColor = icon.color;
        tempColor.a = 1f;
        icon.color = tempColor;

        // Depending on the content of the recipe, we display the corresponding icon
        switch (content)
        {
            case Recipe.Bun:
                icon.sprite = Resources.Load<Sprite>("Icons/bun_over_icon");
                break;
            case Recipe.BunWithCheese:
                icon.sprite = Resources.Load<Sprite>("Icons/bun_cheese_over_icon");
                break;
            case Recipe.BunWithTomato:
                icon.sprite = Resources.Load<Sprite>("Icons/bun_tomato_over_icon");
                break;
            case Recipe.BunWithSteak:
                icon.sprite = Resources.Load<Sprite>("Icons/bun_steak_over_icon");
                break;
            case Recipe.BunWithTomatoAndCheese:
                icon.sprite = Resources.Load<Sprite>("Icons/bun_tomato_cheese_over_icon");
                break;
            case Recipe.BunWithTomatoAndSteak:
                icon.sprite = Resources.Load<Sprite>("Icons/bun_steak_tomato_over_icon");
                break;
            case Recipe.BunWithSteakAndCheese:
                icon.sprite = Resources.Load<Sprite>("Icons/bun_steak_cheese_over_icon");
                break;
            case Recipe.FullBun:
                icon.sprite = Resources.Load<Sprite>("Icons/bun_steak_tomato_cheese_over_icon");
                break;
            default:
                tempColor.a = 0f;
                icon.color = tempColor;
                break;
        }
    }

    // This method is called every frame to update the gameobjects
    public void Update()
    {
        // We place the icons detailling the content of the container to be right over the object in the screen view
        Vector3 screenPos = Camera.main.WorldToScreenPoint(transform.position + transform.up * 1f);
        icon.transform.position = screenPos;
    }
    
    /// <summary>
    /// This method is used to add a fooditem into a container
    /// </summary>
    /// <param name="ingredient">The fooditem that you want to add to the container</param>
    /// <param name="table">The table on which the container is</param>
    public virtual void AddIngredient(FoodItem ingredient, Table table)
    {

        // We get the ingredient corresponding to the FoodItem and throw an error there is none
        Ingredient? newIngredient = ingredient.GetRecipeType();
        if (newIngredient is null)
            throw new UnauthorizedActionException("You can't put this into a container");
        Ingredient newIngredientType = (Ingredient)newIngredient;


        // We instanciate a list with the new ingredient and add the ingredients correspounding to the former content of the container if there was one
        List<Ingredient> all_ingredients = new List<Ingredient> { newIngredientType };
        if (!(content is null))
        {
            Recipe recipe = (Recipe)content;
            all_ingredients.AddRange(RecipeAssembler.Disassemble(recipe));
        }

        // We get the corresponding Prefab (and recipe) corresponding to the new list of ingredients
        System.Tuple<ContainerItem, Recipe> result = recipeAssembler.GetPrefabFromRecipe(all_ingredients);
        ContainerItem newObjectPrefab = result.Item1;

        // We store the position of the former container and destroy it as well as the ingredient
        Transform formerTransform = transform;
        Destroy(gameObject);
        Destroy(ingredient.gameObject);

        // We instanciate the new updated Container item and set its content with the recipe
        ContainerItem newObject = Instantiate(newObjectPrefab, formerTransform.position, formerTransform.rotation);
        newObject.content = result.Item2;

        // We put down the new object on the table
        table.occupied = false;
        table.PutDown(newObject);

        // Finally we add its descriptive icon to the new object
        newObject.AddIcon();

    }


    /// <summary>
    /// This functions is used to add ingredients to another container item 
    /// </summary>
    /// <param name="ingredients">A list of the ingredients to add into the container item</param>
    /// <param name="table">The table on which the container is if case be, null on the contrary</param>
    /// <param name="carrier">The carrier holding the container if case be, null on the contrary</param>
    /// <returns>The updated container</returns>
    public virtual ContainerItem AddIngredient(List<Ingredient> ingredients, Table table=null, GameObject carrier=null)
    {
        // If the container is not empty we add the list of ingredients already in the plate to the list of all the ingredients
        if (!(content is null))
        {
            Recipe recipe = (Recipe)content;
            List<Ingredient> all_ingredients = RecipeAssembler.Disassemble(recipe);
            ingredients.AddRange(all_ingredients);
        }

        // We get the corresponding Prefab (and recipe) corresponding to the new list of ingredients
        System.Tuple<ContainerItem, Recipe> result = recipeAssembler.GetPrefabFromRecipe(ingredients);
        ContainerItem newObjectPrefab = result.Item1;

        // We store the position of the former container and destroy it 
        Transform formerTransform = transform;
        Destroy(gameObject);

        // We instanciate the new updated Container item and set its content with the recipe
        ContainerItem newObject = Instantiate(newObjectPrefab, formerTransform.position, formerTransform.rotation);
        newObject.content = result.Item2;

        // We add its descriptive icon to the new object
        newObject.AddIcon();
        
        // If the container was not on a table, meaning it was held by the carrier, the carrier picks the newObject up and we return the object
        if (table is null)
        {
            newObject.GetPickedUp(carrier);
            return newObject;
        }
        // If the container was on a table, we put down the new object on the table
        table.occupied = false;
        return (ContainerItem)table.PutDown(newObject);
    }

    /// <summary>
    /// The function determining the behavior of a container item burning
    /// </summary>
    /// <returns>The burnt container</returns>
    public override Item Burn()
    {
        // If there is no content, the container doesn't burn and we return it as such
        if (content == null)
            return this;

        // If not, we destroy the old container, instanciate a burntContainer with burnt stuff as a content and return it
        ContainerItem burntContainer = Instantiate(burntPlatePrefab, transform.position, transform.rotation);
        Destroy(gameObject);
        burntContainer.content = Recipe.BurntStuff;
        return burntContainer;
    }
}