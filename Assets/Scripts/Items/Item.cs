﻿using UnityEngine;

/// <summary>
/// This class implements the logic of game items
/// </summary>
/// <remarks>
/// We consider as game items any item that can be carried by the player
/// </remarks>
public abstract class Item : MonoBehaviour
{
    // A boolean, used to store whether the item is picked up or not
    private bool _isPickedUp;
    // A reference to the character's carry slot
    private GameObject _carrier;

    /// <summary>
    /// A function used when the player picks up the item
    /// </summary>
    /// <param name="carrier">The carry slot</param>
    public void GetPickedUp(GameObject carrier)
    {
        _carrier = carrier;
        _isPickedUp = true;
    }

    /// <summary>
    /// A function used when the player puts the item down
    /// </summary>
    public void GetDroppedOnTable()
    {
        _carrier = null;
        _isPickedUp = false;
    }

    // Once per frame, we move the item so that it will follow the carry slot
    void LateUpdate()
    {
        if (_isPickedUp)
            transform.position = _carrier.transform.position;
    }

    /// <summary>
    /// A function used when an item burns
    /// </summary>
    /// <returns>The burnt item</returns>
    public abstract Item Burn();
}
