﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Items.Recipes;
using Constants;

/// <summary>
/// This class implements the logic of the cooking container Item which is to say an container item that can be put on hotplates and will cook.
/// </summary>
/// <remarks>
/// It is still an abstract defining the global logic and behavior of cooking containers.
/// We use classes such as frying pan (and sauce pan in the version 2.0) to implement it, taking into account the specificities of each cooking container (what ingredients it can cook, etc.)
/// </remarks>
public abstract class CookingContainerItem : ContainerItem
{
    // The reference to the prefab of the burnt version of the cooking container
    public CookingContainerItem burntCookingContainerPrefab;

    // Asynchronous action related properties 
    private bool _inUse;
    private ActionProgression _actionProgressionTimer; // References the instance of the action progression timer
    public ActionProgression burningTimerPrefab;       // Prefab for timer when it is burning
    public ActionProgression cookingTimerPrefab;       // Prefab for timer when it is cooking

    private Table _table; // due to the particular nature of the relation between the cooking container and the hotplate, we keep a track of the table the cooking container is on

    // Sound related properties
    private AudioSource _playingSound; // The Audiosource for the sound when the cooking container is used
    public AudioClip fryingSound;      // The audio clip when it is frying
    public AudioClip burningSound;     // The audio clip when it is burning


    // Before the items are instanciated, we link the reference to the actual audio source and set it up
    private void Awake()
    {
        _playingSound = GetComponent<AudioSource>();
        _playingSound.loop = true;
        _playingSound.clip = fryingSound;
    }

    /// <summary>
    /// This method is used to add a fooditem into a cooking container
    /// </summary>
    /// <param name="ingredient">The fooditem that you want to add to the cooking container</param>
    /// <param name="table">The table on which the cooking container is</param>
    public override void AddIngredient(FoodItem ingredient, Table table)
    {
        // If the ingredient is cookable and can be cooked here 
        if (ingredient is ICookable && CanBeCookedHere(ingredient))
        {
            // we store the table and the position of the container
            _table = table;
            Transform formerTransform = transform;

            // We destroy the container and the ingredient and indicate the table is empty
            Destroy(gameObject);
            Destroy(ingredient.gameObject);
            _table.occupied = false;

            // We get the prefab corresponding to the cooked item and instantiate it
            CookingContainerItem newObjectPrefab = CanBeCookedHere(ingredient);
            CookingContainerItem newContainer = Instantiate(newObjectPrefab, formerTransform.position, formerTransform.rotation);

            // We update the content of the container
            List<Ingredient> newContents = new List<Ingredient>();
            Ingredient? currentContent = ingredient.GetRecipeType();
            if(content != null)
                newContents.AddRange(RecipeAssembler.Disassemble(content.Value));
            if(currentContent != null)
                newContents.Add(currentContent.Value);
            newContainer.content = RecipeAssembler.Assemble(newContents);

            // We put down the new container on the table (which will trigger the cooking if we are on a hotplate
            _table.PutDown(newContainer);
        }
        else
            throw new UnauthorizedActionException("You can't cook that, you fool !");
    }


    /// <summary>
    /// When you start cooking an Item, this method is called inside a Coroutine.
    /// Coroutines work by going through an IEnumerator in an event loop, until it
    /// does not yield anymore.
    /// Here it will Cook or burn an item incrementally, making a slider progress through
    /// the action until it is done.
    /// </summary>
    /// <param name="table">The table on which the cookingContainer is placed</param>
    /// <returns>An IEnumerator, to be processed in a Coroutine</returns>
    /// <seealso cref="FoodItem"/>
    public IEnumerator StartCooking(Table table)
    {
        _inUse = true;
        _table = table;
        
        // If the cooking has already started we just set _inUse to be true, which 
        // will resume the cooking process, and we do not start a new one.
        if (!HasCookingStarted() && content != null)
        {
            InitPlayingSound();
            _playingSound.Play();
            if (!RecipeAssembler.IsCookable(content.Value))
            {
                // If the content of our cookingContainer is not Cookable, it shall burn
                SetActionProgression(burningTimerPrefab);
                yield return DoActionForDuration(TimeConstants.BURNING_DURATION, true);
                // If we get here, it means the timer arrived at the end and the table catches fire.
                _table.CatchFire(); 
            }
            else
            {
                // Otherwise, it will cook
                SetActionProgression(cookingTimerPrefab);
                yield return DoActionForDuration(TimeConstants.COOKING_DURATION, false);
                // If we get here, it means the timer arrived at the end and the item gets cooked.
                Cook(_table);
            } 
            InitPlayingSound();
            Destroy(_actionProgressionTimer);
        }
    }
    
    /// <summary>
    /// Cook the contents of the CookingContainerItem
    /// </summary>
    /// <param name="table">The table on which you are cooking</param>
    public virtual void Cook(Table table)
    {
        (bool canCook, CookingContainerItem newObjectPrefab, Recipe? correspondingContent) = GetCookedContainerItem(this);
        if (canCook)
        {
            Transform formerTransform = transform;
            Destroy(gameObject);
            table.occupied = false;
            CookingContainerItem newObject = Instantiate(newObjectPrefab, formerTransform.position, formerTransform.rotation);
            newObject.content = correspondingContent;
            table.PutDown(newObject);
        }
    }

    /// <summary>
    /// A function determining if a fooditem can be cooked in this cooking container
    /// </summary>
    /// <param name="foodItem">The fooditem that we want to check</param>
    /// <returns>The corresponding cooking container item</returns>
    public abstract CookingContainerItem CanBeCookedHere(FoodItem foodItem);


    /// <summary>
    /// This functions returns the cooking container containing the cooked food item 
    /// </summary>
    /// <param name="cookingContainerItem"></param>
    /// <returns>A bool specifying if the object has been cooked, the cooked prefab and the content of the newly generated prefab</returns>
    public abstract (bool, CookingContainerItem, Recipe?) GetCookedContainerItem(CookingContainerItem cookingContainerItem);

    /// <summary>
    /// The function determining the behavior of a cooking container item burning
    /// </summary>
    /// <returns>The burnt cooking container</returns>
    public override Item Burn()
    {
        // If there is no content, the container doesn't burn and we return it as such
        if (content == null)
            return this;

        // If not, we destroy the old container, instanciate a burntCookingContainer with burnt stuff as a content and return it
        CookingContainerItem burntContainer = Instantiate(burntCookingContainerPrefab, transform.position, transform.rotation);
        Destroy(gameObject);
        burntContainer.content = Recipe.BurntStuff;
        return burntContainer;
    }

    /// <summary>
    /// Method used to set the current instance of the action progression timer
    /// </summary>
    /// <param name="prefab">The prefab to instantiate (burning or cooking timer)</param>
    private void SetActionProgression(ActionProgression prefab)
    {
        _actionProgressionTimer = Instantiate(prefab, transform);
    }

    /// <summary>
    /// Lets you know whether the cooking in this CookingContainerItem
    /// has already started or not.
    /// </summary>
    /// <returns>True if it is cooking, false otherwise.</returns>
    /// <remarks>
    /// It there is no _actionProgressionInstance, it means the
    /// contents have not started cooking/burning yet.
    /// If it is already there, it means the contents have started cooking. 
    /// </remarks>
    public bool HasCookingStarted()
    {
        return !!_actionProgressionTimer;
    }

    /// <summary>
    /// A function to indicate the cooking container starts using the hotplate
    /// </summary>
    /// <param name="table">The hotplate being used</param>
    public void StartUsing(Table table)
    {
        _inUse = true;
        _table = table;
    }

    /// <summary>
    /// A function to indicate the container has been picked up from a hotplate (interrupting the asynchronous action)
    /// </summary>
    public void GotPickedUp()
    {
        _inUse = false;
        _table = null;
        _playingSound.Stop();
    }

    /// <summary>
    /// A method to set which clip is playing 
    /// </summary>
    private void InitPlayingSound()
    {
        // Stop the clip, and re-initialize the pitch
        _playingSound.Stop();
        _playingSound.pitch = 1;
        
        // if the cooking container is empty nothing happens
        if (content is null)
            throw new UnauthorizedActionException("Nothing in here");

        // if has a content : either it is not cookable and we play the burning sound, either it is cookable and we play the frying sound
        if (!RecipeAssembler.IsCookable(content.Value))
            _playingSound.clip = burningSound;
        else
            _playingSound.clip = fryingSound;
    }

    /// <summary>
    /// This method is directly related to the StartCooking method.
    /// It is simply a time loop, in which we update the timer's progression
    /// at every step (with a given granularity)
    /// </summary>
    /// <param name="duration">The duration of the action</param>
    /// <param name="pitchUp">A boolean to say whether the pitch of the played sound should get higher in time</param>
    /// <returns>An IEnumerator, which will be processed in a Coroutine</returns>
    private IEnumerator DoActionForDuration(float duration, bool pitchUp)
    {
        for (int i = 0; i < TimeConstants.ANIMATION_TIME_GRANULARITY; i++)
        {
            if (content is null)
                break; // In case we take pan off hotplate, and empty it
            
            // Stop playing the action sound if the Item is not in use anymore
            // For example if we took the CookingContainerItem off the hotplate
            if (!_inUse && _playingSound.isPlaying)
                _playingSound.Stop();

            // If the CookingContainerItem is not in use anymore, wait for it to be in use again
            while (!_inUse)
                yield return new WaitForSeconds(0.2f);

            // We wait for a time delta, and update our timer
            yield return new WaitForSeconds(duration / TimeConstants.ANIMATION_TIME_GRANULARITY);
            _actionProgressionTimer.UpdateProgression(100 / TimeConstants.ANIMATION_TIME_GRANULARITY);
            
            if (pitchUp)
                _playingSound.pitch = (float)(_playingSound.pitch * 1.003);
            
            // If we start cooking again after stopping, play the sound again
            if (_inUse && !_playingSound.isPlaying)
                _playingSound.Play();
        }
    } 
}