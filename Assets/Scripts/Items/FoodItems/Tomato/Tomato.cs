﻿using Items.Recipes;
using UnityEngine;

/// <summary>
/// This class implements the raw tomato
/// </summary>
public class Tomato : FoodItem, IChoppable
{
    // The prefab of the chopped tomato
    public GameObject choppedTomato;

    public override Ingredient? GetRecipeType()
    {
        // No ingredient needed
        return null;
    }

    /// <summary>
    /// A function to chop the food item
    /// </summary>
    /// <returns>A prefab of the chopped version of the food item</returns>
    public GameObject GetChopped()
    {
        Destroy(gameObject);
        GameObject newObject = Instantiate(choppedTomato, transform.position, transform.rotation);
        return newObject;
    }
}
