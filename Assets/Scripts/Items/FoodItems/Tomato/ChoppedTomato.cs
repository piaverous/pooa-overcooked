﻿using Items.Recipes;

/// <summary>
/// This class implements the chopped tomato
/// </summary>
public class ChoppedTomato : FoodItem
{
    public override Ingredient? GetRecipeType()
    {
        return Ingredient.CutTomato;
    }
}
