﻿using System;
using System.Collections;
using Constants;
using Items.Recipes;
using UnityEngine;

/// <summary>
/// This class implements the logic of food items
/// </summary>
public abstract class FoodItem : Item
{
    // The prefab of the burnt food item
    public FoodItem burntIngredientPrefab;
    
    // The progression of the actions that can be performed on the food item
    private ActionProgression _actionProgressionTimer;
    
    // Boolean, used to store if the food item is being chopped
    private bool _beingChopped;
    // The table (if exists) on which the food item is
    private Table _table;
    
    /// <summary>
    /// This function returns the recipe of the food item, if there is one
    /// </summary>
    /// <returns>Recipe</returns>
    public abstract Nullable<Ingredient> GetRecipeType();

    /// <summary>
    /// Burn this FoodItem
    /// </summary>
    /// <returns>The instance of the burnt FoodItem</returns>
    public override Item Burn()
    {
        // If the item already burnt, it can't burnt any more
        if (this is BurntIngredient)
            return this;
        // Else we instantiate the burnt item and destroy the previous one
        FoodItem burntObject = Instantiate<FoodItem>(burntIngredientPrefab, transform.position, transform.rotation); 
        Destroy(gameObject);
        return burntObject;
    }

    /// <summary>
    /// When you start cooking an Item, this method is called inside a Coroutine.
    /// Coroutines work by going through an IEnumerator in an event loop, until it
    /// does not yield anymore.
    /// 
    /// Here it will Chop an item incrementally, making a slider progress through
    /// the action until it is done.
    /// </summary>
    /// <param name="timerPrefab">The action progression timer prefab</param>
    /// <param name="choppingSound">The chopping action sound</param>
    /// <returns>An IEnumerator, which will be processed in a Coroutine</returns>
    /// <seealso cref="CookingContainerItem"/>
    public IEnumerator StartChopping(ActionProgression timerPrefab, AudioSource choppingSound)
    {
        _beingChopped = true;
        // If the item is not chopped and is choppable, we start chopping it
        if (!HasChoppingStarted() && this is IChoppable)
        {
            // Instantiate the timer, and loop for all time deltas, updating the timer
            // at every step
            _actionProgressionTimer = Instantiate(timerPrefab, transform);
            for (int i = 0; i < TimeConstants.ANIMATION_TIME_GRANULARITY; i++)
            {
                
                // Stop playing the chopping sound if the FoodItem is not being chopped anymore
                if (!_beingChopped && choppingSound.isPlaying)
                    choppingSound.Stop();
                
                // While the FoodItem is not being chopped, wait for it to be chopped again
                // to resume the loop.
                while (!_beingChopped)
                    yield return new WaitForSeconds(0.2f);
                
                // Wait for a time delta, and update the action progress timer accordingly
                yield return new WaitForSeconds(TimeConstants.CHOPPING_DURATION / TimeConstants.ANIMATION_TIME_GRANULARITY);
                _actionProgressionTimer.UpdateProgression(100 / TimeConstants.ANIMATION_TIME_GRANULARITY);
                
                // Play the sound if the FoodItem is being chopped 
                if (_beingChopped && !choppingSound.isPlaying)
                    choppingSound.Play();
            }
            // Update the table with the copped version of the Choppable Item
            _table.putItem = ((IChoppable) _table.putItem).GetChopped().GetComponent<Item>();
            Destroy(_actionProgressionTimer);
            choppingSound.Stop();
        }
    }
    
    /// <summary>
    /// Lets you know whether the chopping process has started or not
    /// </summary>
    /// <returns>True if it is being chopped, false otherwise.</returns>
    /// <seealso cref="CookingContainerItem"/>
    public bool HasChoppingStarted()
    {
        return !!_actionProgressionTimer;
    }

    /// <summary>
    /// Pick up chopping after a stop
    /// </summary>
    /// <param name="table">A reference to the table you are chopping on</param>
    public void RestartChopping(Table table)
    {
        _beingChopped = true;
        _table = table;
    }
    
    /// <summary>
    /// Stop chopping of the food item
    /// </summary>
    public void StopChopping() {
        _beingChopped = false;
    }

    /// <summary>
    /// A function to indicate the food item has been picked up (interrupting the asynchronous action)
    /// </summary>
    public void GotPickedUp()
    {
        StopChopping();
        _table = null;
    }
}
