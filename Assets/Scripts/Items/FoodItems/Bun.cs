﻿using Items.Recipes;

/// <summary>
/// This class implements the bun
/// </summary>
public class Bun : FoodItem
{
    public override Ingredient? GetRecipeType()
    {
        return Ingredient.Bun;
    }
}
