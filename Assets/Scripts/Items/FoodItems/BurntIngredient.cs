using Items.Recipes;

/// <summary>
/// This class implements the burnt Ingredient as a foodItem.
/// </summary>
/// <remarks>
/// It is generated when an object catches fire and can only be throw away or deliver but cannot be mixed with any other ingredients thanks to the recipe assemble
/// </remarks>
public class BurntIngredient : FoodItem
{
    public override Ingredient? GetRecipeType()
    {
        return Ingredient.BurntIngredient;
    }
}