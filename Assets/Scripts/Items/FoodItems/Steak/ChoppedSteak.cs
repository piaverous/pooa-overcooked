﻿using System;
using Items.Recipes;

/// <summary>
/// This class implements the chopped steak
/// </summary>
public class ChoppedSteak : FoodItem, ICookable
{
    public override Nullable<Ingredient> GetRecipeType()
    {
        return Ingredient.ChoppedSteak;
    }
}
