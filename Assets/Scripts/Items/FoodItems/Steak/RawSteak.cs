﻿using System;
using Items.Recipes;
using UnityEngine;

/// <summary>
/// This class implements the raw steak
/// </summary>
public class RawSteak : FoodItem, IChoppable
{
    // The prefab of the chopped steak
    public GameObject choppedSteak;

    /// <summary>
    /// A function to chop the food item
    /// </summary>
    /// <returns>A prefab of the chopped version of the food item</returns>
    public GameObject GetChopped()
    {
        Destroy(gameObject);
        GameObject newObject = Instantiate(choppedSteak, transform.position, transform.rotation);
        return newObject;
    }
    
    public override Nullable<Ingredient> GetRecipeType()
    {
        // No ingredient needed
        return null;
    }
}
