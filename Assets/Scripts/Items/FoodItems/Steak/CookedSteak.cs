﻿using System;
using Items.Recipes;

/// <summary>
/// This class implements the cooked steak
/// </summary>
public class CookedSteak : FoodItem
{
    public override Nullable<Ingredient> GetRecipeType()
    {
        return Ingredient.CookedSteak;
    }
}
