﻿using UnityEngine;

/// <summary>
/// This interface sets the methods for all choppable food items
/// </summary>
interface IChoppable
{
    /// <summary>
    /// A function to chop the food item
    /// </summary>
    /// <returns>A prefab of the chopped version of the food item</returns>
    GameObject GetChopped();
}

