﻿using Items.Recipes;

/// <summary>
/// This class implements the chopped cheese
/// </summary>
public class ChoppedCheese : FoodItem
{
    public override Ingredient? GetRecipeType()
    {
        return Ingredient.CutCheese;
    }
}
