﻿using Items.Recipes;
using UnityEngine;

/// <summary>
/// This class implements the raw cheese
/// </summary>
public class Cheese : FoodItem, IChoppable
{
    // The prefab of the chopped cheese
    public GameObject choppedCheese;

    public override Ingredient? GetRecipeType()
    {
        // No ingredient needed
        return null;
    }

    /// <summary>
    /// A function to chop the food item
    /// </summary>
    /// <returns>A prefab of the chopped version of the food item</returns>
    public GameObject GetChopped()
    {
        Destroy(gameObject);
        GameObject newObject = Instantiate(choppedCheese, transform.position, transform.rotation);
        return newObject;
    }
}
