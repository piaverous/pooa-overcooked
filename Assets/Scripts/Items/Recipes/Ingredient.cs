namespace Items.Recipes
{
    /// <summary>
    /// An enum defining all the atomatic ingredient that can form a recipe 
    /// </summary>
    /// <remarks>
    /// This correspounds to the atomic items that can be put in a cooking container 
    /// </remarks>
    public enum Ingredient
    {
        CutTomato,
        CutCheese,
        ChoppedSteak, 
        CookedSteak, 
        Bun,

        BurntIngredient
    }
}