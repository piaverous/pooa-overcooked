using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Items.Recipes
{
    /// <summary>
    /// A class defining the rules to assemble and disassemble recipes from and into ingredients
    /// </summary>
    public class RecipeAssembler : MonoBehaviour
    {

        // All the prefabs for the specific recipes
        public ContainerItem CutTomatoPrefab;
        public ContainerItem CutCheesePrefab;
        public ContainerItem CookedSteakPrefab;
        public ContainerItem BunPrefab;
        public ContainerItem BunWithTomatoPrefab;
        public ContainerItem BunWithCheesePrefab;
        public ContainerItem BunWithTomatoAndCheesePrefab;
        public ContainerItem BunWithSteakPrefab;
        public ContainerItem BunWithSteakAndCheesePrefab;
        public ContainerItem BunWithTomatoAndSteakPrefab;
        public ContainerItem FullBunPrefab;



        /// <summary>
        /// The function assembling several ingredients into a recipe
        /// </summary>
        /// <param name="ingredients">The list of ingredients</param>
        /// <returns>The corresponding recipe</returns>
        public static Recipe Assemble(List<Ingredient> ingredients)
        {
            // We create bools to identify the presence of our different ingredients
            bool hasChoppedSteak = ingredients.Contains(Ingredient.ChoppedSteak);
            bool hasSteak = ingredients.Contains(Ingredient.CookedSteak);
            bool hasTomato = ingredients.Contains(Ingredient.CutTomato);
            bool hasBun = ingredients.Contains(Ingredient.Bun);
            bool hasCheese = ingredients.Contains(Ingredient.CutCheese);


            //Then we iterate throught the number of ingredients and their nature to return the good recipe
            if (ingredients.Count() == 4)
            {
                if (hasBun && hasSteak && hasTomato && hasCheese) 
                    return Recipe.FullBun;
            }

            if (ingredients.Count() == 3)
            {
                if (hasBun)
                {
                    if (hasTomato)
                    {
                        if (hasCheese)
                        {
                            return Recipe.BunWithTomatoAndCheese;
                        }
                        if (hasSteak)
                        {
                            return Recipe.BunWithTomatoAndSteak;
                        }
                    }
                    else if (hasCheese && hasSteak) 
                    {
                        return Recipe.BunWithSteakAndCheese;
                    }
                }
                else
                {
                    return Recipe.TomatoWithSteakAndCheese;
                }
            }

            if (ingredients.Count() == 2)
            {
                if (hasBun)
                {
                    if (hasTomato)
                        return Recipe.BunWithTomato;
                    if (hasSteak)
                        return Recipe.BunWithSteak;
                    if (hasCheese)
                        return Recipe.BunWithCheese;
                }

                if (hasTomato)
                {
                    if (hasSteak)
                        return Recipe.TomatoWithSteak;
                    if (hasCheese)
                        return Recipe.TomatoWithCheese;
                }
                if (hasCheese)
                    return Recipe.SteakWithCheese;
            }

            if (ingredients.Count() == 1)
            {
                if (hasBun)
                    return Recipe.Bun;
                if (hasTomato) 
                    return Recipe.CutTomato;
                if (hasSteak)
                    return Recipe.CookedSteak;
                if (hasChoppedSteak)
                    return Recipe.ChoppedSteak;
                if (hasCheese)
                    return Recipe.CutCheese;
            }
            // if the list of ingredient doesn't match with anyof the recipes, it means the assembly is wrong and we throw an error
            throw new UnauthorizedActionException("This is not a known recipe. Could not assemble.");
        }

        /// <summary>
        /// The method returning the prefab corresponding to the assembled recipe
        /// </summary>
        /// <param name="ingredients">The list of ingredients composing the recipe</param>
        /// <returns>The assemble container prefab and the corresponding recipe</returns>
        public Tuple<ContainerItem, Recipe> GetPrefabFromRecipe(List<Ingredient> ingredients)
        {
            // We assemble the ingredients to get the corresponding recipe 
            Recipe recipeType = Assemble(ingredients);

            // Then we instanciate and set the value of gameObjectPrefab according to the recipe found
            ContainerItem gameObjectPrefab;
            switch (recipeType)
            {
                case Recipe.Bun:
                    gameObjectPrefab = BunPrefab;
                    break;
                case Recipe.CutTomato:
                    gameObjectPrefab = CutTomatoPrefab;
                    break;
                case Recipe.CutCheese:
                    gameObjectPrefab = CutCheesePrefab;
                    break;
                case Recipe.CookedSteak:
                    gameObjectPrefab = CookedSteakPrefab;
                    break;
                case Recipe.BunWithSteak:
                    gameObjectPrefab = BunWithSteakPrefab;
                    break;
                case Recipe.FullBun:
                    gameObjectPrefab = FullBunPrefab;
                    break;
                case Recipe.BunWithTomato:
                    gameObjectPrefab = BunWithTomatoPrefab;
                    break;
                case Recipe.BunWithTomatoAndCheese:
                    gameObjectPrefab = BunWithTomatoAndCheesePrefab;
                    break;
                case Recipe.BunWithCheese:
                    gameObjectPrefab = BunWithCheesePrefab;
                    break;
                case Recipe.BunWithSteakAndCheese:
                    gameObjectPrefab = BunWithSteakAndCheesePrefab;
                    break;
                case Recipe.BunWithTomatoAndSteak:
                    gameObjectPrefab = BunWithTomatoAndSteakPrefab;
                    break;
                default:
                    // if it doesn't match any of the case, the recipe is not allowed
                    throw new UnauthorizedActionException("This is not a known recipe. Could not get prefab.");
            }
            // FInally we return the corresponding results
            return new Tuple<ContainerItem, Recipe>(gameObjectPrefab, recipeType);
        }


        /// <summary>
        /// A method to obtain the list of ingredients composing a recipe
        /// </summary>
        /// <param name="recipe">The recipe to decompose into its ingredients</param>
        /// <returns>The corresponding list of ingredients</returns>
        public static List<Ingredient> Disassemble(Recipe recipe)
        {
            // Depending on the recipe, we return the correponding list of ingredients
            switch (recipe)
            {
                case Recipe.Bun:
                    return new List<Ingredient> {Ingredient.Bun};
                case Recipe.CutTomato:
                    return new List<Ingredient> { Ingredient.CutTomato};
                case Recipe.CutCheese:
                    return new List<Ingredient> { Ingredient.CutCheese };
                case Recipe.CookedSteak:
                    return new List<Ingredient> { Ingredient.CookedSteak};
                case Recipe.FullBun:
                    return new List<Ingredient> { Ingredient.Bun, Ingredient.CutTomato, Ingredient.CookedSteak, Ingredient.CutCheese};
                case Recipe.BunWithSteak:
                    return new List<Ingredient> { Ingredient.Bun, Ingredient.CookedSteak};
                case Recipe.BunWithCheese:
                    return new List<Ingredient> { Ingredient.Bun, Ingredient.CutCheese};
                case Recipe.BunWithSteakAndCheese:
                    return new List<Ingredient> { Ingredient.Bun, Ingredient.CookedSteak, Ingredient.CutCheese};
                case Recipe.BunWithTomatoAndSteak:
                    return new List<Ingredient> { Ingredient.Bun, Ingredient.CookedSteak, Ingredient.CutTomato};
                case Recipe.BunWithTomatoAndCheese:
                    return new List<Ingredient> { Ingredient.Bun, Ingredient.CutCheese, Ingredient.CutTomato};
                case Recipe.BunWithTomato:
                    return new List<Ingredient> { Ingredient.Bun, Ingredient.CutTomato};
                case Recipe.TomatoWithSteak:
                    return new List<Ingredient> { Ingredient.CutTomato, Ingredient.CookedSteak};
                case Recipe.BurntStuff:
                    return new List<Ingredient> { Ingredient.BurntIngredient };

                // if this doesn't match any known recipe, we throw an error
                default:
                    throw new UnauthorizedActionException("This is not a known recipe. Could not disassemble.");
            }
        }

        /// <summary>
        /// A method allowing to know if a recipe is cookable
        /// </summary>
        /// <param name="recipe">The recipe we want to check</param>
        /// <returns>True if the recipe is cookable, false if not</returns>
        public static bool IsCookable(Recipe recipe)
        {
            Recipe[] cookables = { Recipe.ChoppedSteak };
            if (cookables.Contains(recipe))
                return true;
            return false;
        }
    }
}