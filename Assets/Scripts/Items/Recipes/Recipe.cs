namespace Items.Recipes
{
    /// <summary>
    /// An enum defining all the recipes that can be formed
    /// </summary>
    /// <remarks>
    /// This correspounds to the items that can be contained in container
    /// </remarks>
    public enum Recipe
    {
        // Ingredients
        CutTomato,
        CutCheese,
        ChoppedSteak, 
        CookedSteak, 
        Bun,
            
            
        // Burger Recipes
        TomatoWithSteak,
        TomatoWithCheese,
        SteakWithCheese,
        TomatoWithSteakAndCheese,
        BunWithTomato,
        BunWithSteak,
        BunWithCheese, 
        BunWithTomatoAndCheese,
        BunWithTomatoAndSteak,
        BunWithSteakAndCheese,
        FullBun,


        // Burnt content
        BurntStuff
    }
}