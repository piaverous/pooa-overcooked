﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class represents the screen shown at the end of a level
/// </summary>
public class EndGameScreen : MonoBehaviour
{
    // Display related properties
    public Text scoreText;
    public Text title;
    private int _currentLevel;

    /// <summary>
    /// This function sets the text that displays the player's score
    /// on a given level
    /// </summary>
    /// <param name="score">The score that the player achieved on this level</param>
    public void SetScore(int score)
    {
        scoreText.text = $"Your score: {score}";
    }
    
    /// <summary>
    /// Function used to let this object know what the current level is
    /// </summary>
    /// <param name="level">The number of the current level. Ex. 1 for level 1</param>
    public void SetCurrentLevel(int level)
    {
        _currentLevel = level;
        title.text = $"Well done ! You completed level {_currentLevel}!";
    }
}
