﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// This class is used to bind the buttons of the menu to different
/// scenes of the game.
/// </summary>
public class LoadOnClick : MonoBehaviour
{
    /// <summary>
    /// This function loads a scene into the game. It is called when
    /// navigating through the game in the main menu
    /// </summary>
    /// <param name="level">Name of the level you wish to load</param>
    public void LoadScene(string level)
    {
        SceneManager.LoadScene(level);
    }
}
