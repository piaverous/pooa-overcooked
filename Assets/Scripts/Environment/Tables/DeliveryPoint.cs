﻿using Controllers;
using UnityEngine;

/// <summary>
/// This class implements the delivery point logic as an automatic action table : everything object put down on the delivery point will be delivered if it is the right king of object.
/// </summary>
public class DeliveryPoint : AutomaticActionTable
{
    // The Audiosource for the delivery sound when a container is delivered
    private AudioSource deliverySound;

    // The gameController allowing to handle the recipes 
    public GameController gameController;


    // Before the items are instanciated, we link the reference to the actual audio source
    private void Awake()
    {
        deliverySound = GetComponent<AudioSource>();
    }

    /// <summary>
    /// We execute the task of the delivery table and deliver the item being put down
    /// </summary>
    /// <param name="item">The item to delivered </param>
    /// <param name="carrier">The carrier putting down the object to execute the task if there is one, null if there isn't</param>
    /// <returns>The item being put down if it cannot be delivered, null on the contrary</returns>
    public override Item ExecuteTask(Item item, GameObject carrier=null)
    {
        // If the item is a container item but not a cooking containerItem (which is to say if it is a plate), it can be deluvered
        if (item is ContainerItem && !(item is CookingContainerItem))
        {
            // In that case, we process the delivery, destroy the game object
            gameController.GetRecipeController().ProcessDelivery((ContainerItem)item);
            Destroy(item.gameObject);
            // We reset the tabe as empty 
            occupied = false;
            putItem = null;
            // We play the delivery sound, update the number of plate in game and return null
            deliverySound.Play();
            PlateSource.platesinGame--;
            return null;
        }
        // if it cannot be delivered, the item is picked up back by the carrier and we throw an exception to inform the player he cannot perform this action
        PickUp(carrier);
        throw new UnauthorizedActionException("You can't deliver that !");
    }
}