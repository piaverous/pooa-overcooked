﻿using System;
using UnityEngine;

/// <summary>
/// This class implements the hotplate logic as an automatic action table
/// </summary>
public class Hotplate : AutomaticActionTable
{
    // The prefab of the 
    public CookingContainerItem starterCookingContainer;

    // This method is called right when the object is instanciated
    protected override void Start()
    {
        // We initialize the table as a classic table, specify it is asynchrone
        base.Start();
        isAsynchronous = true;
        // We instantiate the corresponding cooking container and put it down 
        CookingContainerItem cookingContainerItem = Instantiate(starterCookingContainer, transform.position + new Vector3(0f, 0.8f, 0f), transform.rotation * new Quaternion(0, 0, 0, 0));
        PutDown(cookingContainerItem);
    }

    /// <summary>
    /// The method called when wd put down an object on the hotplate
    /// </summary>
    /// <param name="item">The item being put down</param>
    /// <param name="carrier">The carrier putting down the object if there is one, null if there isn't</param>
    /// <returns>The item that the carrier should pick up if need be</returns>
    public override Item PutDown(Item item, GameObject carrier = null)
    {
        // If the hotplate is already occupied 
        if (occupied)
        {
            // We check that the put item is necessarily by a cooking container item
            if (!(putItem is CookingContainerItem))
                throw new Exception("what happened here?");
            // If the new item is a food item, we add it to the container item
            if (item is FoodItem)
            {
                ((CookingContainerItem)putItem).AddIngredient((FoodItem)item, this);
                return null;
            }
            // If it is a container item, we transfer the content between the two containers and return the updated held container
            if (item is ContainerItem)
            {
                return TransferBetweenContainers((ContainerItem)putItem, (ContainerItem)item, carrier);
            }
            //if it is anything else, it is not posssible to put it down so we throw an error
            throw new UnauthorizedActionException("you can't do that");
        }
        // If it is not occupied, the only item we can put down is a cooking container 
        if (item is CookingContainerItem)
        {
            // In that case, we put down the container on the automatic action table, triggering the execute task method as well
            base.PutDown(item, carrier);
            return null;
        }
        throw new UnauthorizedActionException("You can't put that on a hotplate you idiot");
    }

    /// <summary>
    /// This is the method triggered when we put down a cooking container item on the table
    /// </summary>
    /// <param name="item">The item on which to execute the task</param>
    /// <param name="carrier">The carrier triggering the action by putting down the item</param>
    /// <returns>Null in all cases</returns>
    public override Item ExecuteTask(Item item, GameObject carrier = null)
    {
        // If the item is a cooking container item, we cook its content
        if (item is CookingContainerItem)
        {
            ((CookingContainerItem)item).Cook(this);
            return null;
        }
        // If it isn't it is a forbidden action and we throw en exception
        throw new UnauthorizedActionException("You can't cook that you idiot");
    }

    /// <summary>
    /// The method being called when an object is picked up from a hotplate
    /// </summary>
    /// <param name="carrier">The carrier picking up the object</param>
    /// <returns>The object to pick up</returns>
    public override Item PickUp(GameObject carrier)
    {
        // We make sure that the item is a cooking container as it is the only kind of item allowed here
        if (!(putItem is CookingContainerItem))
            throw new Exception("How did you put that here ?!");

        // The cooking container gets picked up interrupting the cooking action
        ((CookingContainerItem) putItem).GotPickedUp();

        // We return the cooking container
        return base.PickUp(carrier);
    }

    /// <summary>
    /// The coroutine executed by the asynchronous task
    /// </summary>
    /// <param name="item">The item on which the action is performed</param>
    /// <param name="carrier">The carrier at the origin of the action if there is one, null if there isn't</param>
    /// <returns>The coroutine being executed</returns>
    public override Coroutine AsyncAction(Item item, GameObject carrier = null)
    {
        // if the item is a cooking container item, we start using it and launch the coroutine
        if (item is CookingContainerItem containerItem)
        {
            containerItem.StartUsing(this);
            if (!containerItem.HasCookingStarted() && containerItem.content != null)
                return StartCoroutine(containerItem.StartCooking(this));
        }
        return null;
    }
}