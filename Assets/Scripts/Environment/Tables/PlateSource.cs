﻿using UnityEngine;

/// <summary>
/// This class implements the logic of the plate source. 
/// </summary>
/// <remarks>
/// This class was meant as an intermediary step before implementing the reapparition of dirty plates a few seconds after their delivery and the whole cleaning logic.
/// Due to time constraint and prioritization, this game meachnics were not included in this MVP.
/// As a result, the plate source behaves almost exaclty like the food sources currently. However this did not result in a common architecture due this temporary nature.
/// </remarks>
public class PlateSource : TriggerableActionTable
{
    // The prefab of the plate to instanciate 
    public ContainerItem plate;

    // A static counter keeping of the number of plates in Gamme
    public static int platesinGame;

    protected override void Start()
    {
        base.Start();
        platesinGame = 0; // At the start of a level, set the number of present plates back to zero
    }

    /// <summary>
    /// The method being called when the table is used to create a new instance of the plate
    /// </summary>
    public override void Use()
    {
        // If the table is occupied we cannot generate another plate
        if (occupied)
            throw new UnauthorizedActionException("The table is occupied!");

        // If we already generated the maximal amount of ingame plate we cannot generate another plate
        if (platesinGame >= Constants.GameConstants.PLATE_NUMBER_MAX)
            throw new UnauthorizedActionException("There are already too many plates");

        // We get the Use context using the base method, instantiate the new plate, put it down on the table and increment the ingame plate counter
        base.Use();
        ContainerItem newItem = Instantiate(plate, transform.position + new Vector3(0f, 0.8f, 0f), transform.rotation * new Quaternion(0, 0, 0, 0));
        PutDown(newItem);
        platesinGame++;
    }

}