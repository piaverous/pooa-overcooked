﻿using Constants;
using UnityEngine;

/// <summary>
/// The class implementing the food source table as a triggerable action table
/// </summary>
public class FoodSource : TriggerableActionTable
{
    // The prefab of the food item to generate 
    public FoodItem ingredient;

    /// <summary>
    /// The method being called when the table is used to create a new instance of the corresponding ingredient
    /// </summary>
    public override void Use()
    {
        // We create the context of the Use() method using the base method
        base.Use();

        // Then if the table is not occupied, we instanciate a new object and put it down on the table
        if (!occupied)
        {
            FoodItem newItem = Instantiate(
                ingredient, 
                transform.position + new Vector3(0f, SizeConstants.TABLE_HEIGHT,0f), 
                transform.rotation * new Quaternion(0,0,0,0)
            );
            PutDown(newItem);
        }
    }
}