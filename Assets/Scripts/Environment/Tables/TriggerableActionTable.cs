﻿/// <summary>
/// This abstract class allows to implement the logic of triggerable action table (a table the player can interact with to trigger specific action)
/// </summary>
public abstract class TriggerableActionTable : Table
{
    /// <summary>
    /// The method called when the player triggers the table action
    /// </summary>
    public virtual void Use()
    {
        // If the table is on fire we cannot use it 
        if (onFire)
            throw new UnauthorizedActionException("Holy sh*t the table is on fire!!!!");

        // If it is asynchronous, we indicate the table is being used and start the UseAction coroutine
        if (isAsynchronous)
        {
            _inUse = true;
            UseAction();
        }
    }

    /// <summary>
    /// A method to interrupt the UseAction coroutine if the player isn't facing the table anymore
    /// </summary>
    /// <param name="Switch">A bool indicating if the table is in focus or not</param>
    public override void Highlight(bool Switch)
    {
        // We keep the Highlighting functionnality of the table
        base.Highlight(Switch);
        // If the table is not in focus we stop its usage
        if (!Switch)
            StopUse();
    }

    /// <summary>
    /// A method to stop the usage of the table 
    /// </summary>
    public virtual void StopUse()
    {
        // we set the inUse property to false which will modify the behavior of the coroutine use action
        _inUse = false;
    }

    /// <summary>
    /// The method defining the coroutine being executed if the table has an asynchronous action
    /// </summary>
    /// <remarks>
    /// As only some of the triggerable action tables have asynchronous actions, we set this method as a virtual method, allowing some of the children (the asyncrhonous ones) to implement it but without forcing all of them to implement it
    /// We throw an error for the developpers to know if they forgot to implement UseAction method in an asynchronous table.
    /// </remarks>
    public virtual void UseAction()
    {
        throw new System.NotImplementedException();
    }
}