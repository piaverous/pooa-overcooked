using System;
using UnityEngine;

/// <summary>
/// This abstract class allows to implement the logic of automatic action table (a table the player automatically triggering specific action upon putting down of an object)
/// </summary>
public abstract class AutomaticActionTable : Table
{

    /// <summary>
    /// The method called wen an item is put down on the table
    /// </summary>
    /// <param name="item">The item being put down</param>
    /// <param name="carrier">The carrier putting down the object if there is one, null on the contrary</param>
    /// <returns>The new item carried by the player if need be</returns>
    public override Item PutDown(Item item, GameObject carrier=null)
    {
        // We put down the object on the table
        base.PutDown(item, carrier);

        // Then we execute the automatic action of the table
        // If the table is asynchronous we declare the table in use, and run the async action 
        if (isAsynchronous)
        {
            _inUse = true;
            AsyncAction(item, carrier);
            return null;
        }
        // If not we just execute the automatic task
        return ExecuteTask(item, carrier);
    }

    /// <summary>
    /// An abstract method defining the task executed by the automatic table
    /// </summary>
    /// <param name="item">The item on which to execute the task</param>
    /// <param name="carrier">The carrier triggerring the action by putting down the object</param>
    /// <returns>The item that the player should carry if need be</returns>
    public abstract Item ExecuteTask(Item item, GameObject carrier=null);

    /// <summary>
    /// The method defining the coroutine being executed if the table has an asynchronous action
    /// </summary>
    /// <remarks>
    /// As only some of the triggerable action tables have asynchronous actions, we set this method as a virtual method, allowing some of the children (the asyncrhonous ones) to implement it but without forcing all of them to implement it
    /// We throw an error for the developpers to know if they forgot to implement UseAction method in an asynchronous table.
    /// </remarks>
    /// <returns></returns>
    public virtual Coroutine AsyncAction(Item item, GameObject carrier=null)
    {
        throw new NotImplementedException();
    }
}