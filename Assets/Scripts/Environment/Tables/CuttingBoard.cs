﻿using UnityEngine;

/// <summary>
/// A class implementing the cutting boardslogic as a triggerable action table
/// </summary>
public class CuttingBoard : TriggerableActionTable
{
    // The Audiosource for the cutting sound when the cutting board is used
    private AudioSource _cuttingSound;

    // Before the items are instanciated, we link the reference to the actual audio source and set it up
    private void Awake()
    {
        _cuttingSound = GetComponent<AudioSource>();
        _cuttingSound.loop = true;
    }

    // The method called when the object is instanciated. The action of the cutting board being asynchronous, we set the table as an asynchronous one
    protected override void Start()
    {
        isAsynchronous = true;
    }
    

    /// <summary>
    /// The method called when the object is picked up
    /// </summary>
    /// <param name="carrier">The carrier picking up the object</param>
    /// <returns>The object being picked up</returns>
    public override Item PickUp(GameObject carrier)
    {
        // If the object is a choppable Fooditem, it means the chopping action hasn't finished yet so we call the gotPickedUp method to interrupt the action
        if(putItem is IChoppable && putItem is FoodItem)
            ((FoodItem) putItem).GotPickedUp();
        // Then we pick up the object normally 
        return base.PickUp(carrier);
    }

    /// <summary>
    /// The method triggered when the player uses the cutting board to chop a food item
    /// </summary>
    public override void Use()
    {
        // We make sure that there is an object on the table and it is choppable
        if (!occupied)
            throw new UnauthorizedActionException("There is nothing to chop!");
        if (!(putItem is IChoppable))
            throw new UnauthorizedActionException("You can't chop that!");
        // Then we use the table to chop the time and play the cutting sound
        base.Use();
        _cuttingSound.Play();
    }

    /// <summary>
    /// The asynchronous action happening when we use the table
    /// </summary>
    public override void UseAction()
    {
        // If the item is a choppable food item we start chopping the item and launch the coroutine
        if (putItem is FoodItem && putItem is IChoppable)
        {
            FoodItem foodItem = (FoodItem) putItem;
            if (!((FoodItem) putItem).HasChoppingStarted())
                 StartCoroutine(foodItem.StartChopping(SliderPrefab, _cuttingSound));
            else foodItem.RestartChopping(this);
        }
    }

    /// <summary>
    /// A method to interrupt the chopping
    /// </summary>
    public override void StopUse()
    {
        // if the object is a choppable fooditem, we stop the chopping 
        if (putItem is FoodItem && putItem is IChoppable)
        {
            FoodItem foodItem = (FoodItem) putItem;
            foodItem.StopChopping();
        }
        // Then we specify that the table isn't used anymore
        base.StopUse();
    }
}