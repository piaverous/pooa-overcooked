﻿using System.Collections;
using System.Collections.Generic;
using Constants;
using Items.Recipes;
using UnityEngine;

/// <summary>
/// This class implements all the scripts relative to a basic table. As such, it is also the parent of all other table elements.
/// </summary>
/// <remarks>
/// As a result it should implement all the logic of putting down and pickcing up objects as well as every fire related method for the tables.
/// Finally as both triggerable and automatic action table can be realize action that are not instantaneous, it deals with Progression logic as well.
/// </remarks>
public class Table : MonoBehaviour
{
    // Interaction related properties : we higlight the table when it is in focus
    private Renderer _rend;
    private Color _colorBase = new Color(0.5f, 0.4f, 0.25f);
    private Color _colorFocus = new Color(0.75f, 0.65f, 0.4f);

    // Item related properties
    public bool occupied;
    public Item putItem; // A reference to the item on the table if there is one

    // Fire related properties
    protected bool onFire; 
    public GameObject firePrefab; // The gameobject of the fire : it is the reference to what object should be instanciated if the table catches fire
    private GameObject _burningFire; // A reference to the actual fire object if the table is burning
    protected List<Table> adjacentTables; // A list of all adjacent tables where the fire can propagate


    // Initilization related properties : some table can start with an extinguisher on them 
    public Extinguisher ExtinguisherPrefab; // The gameobject of the extinguisher : it is the reference to what object should be instanciated if there is an extinguisher
    public bool startsWithExtinguisher; 

    // Progressive action related properties
    public ActionProgression SliderPrefab; // The gameobject of the slider  
    protected bool _inUse; 
    protected bool isAsynchronous;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        // If the table is to start with an extinguisher, we create one and put it down on the table
        if (startsWithExtinguisher)
        {
            Extinguisher extinguisher = Instantiate(ExtinguisherPrefab, transform.position + new Vector3(0f, 0.8f, 0f), transform.rotation * Quaternion.Euler(0, -90, 90));
            PutDown(extinguisher);
        }

        // Finally we find the adjacent tables and store them
        adjacentTables = FindAdjacentTables();
    }

    /// <summary>
    /// This function is used to higlight the table if it is in focus
    /// </summary>
    /// <param name="Switch">A boolean indicating whether the table is in focus</param>
    public virtual void Highlight(bool Switch)
    {
        _rend = transform.gameObject.GetComponent<Renderer>();
        Material tableTopMaterial = _rend.materials[1];
        if (Switch)
            tableTopMaterial.color = _colorFocus;
        else
            tableTopMaterial.color = _colorBase;
    }

    /// <summary>
    /// A function to put down an item on a table 
    /// </summary>
    /// <param name="item">The item being put down</param>
    /// <param name="carrier">The carrier putting down the object if there is one. By default, it is null</param>
    /// <returns>The item returned by the table to the carrier if need be, null in most cases</returns>
    public virtual Item PutDown(Item item, GameObject carrier=null)
    {
        // If the table is on fire, we can't put down the item on it
        if (onFire)
            throw new UnauthorizedActionException("Holy sh*t the table is on fire!!!!");

        // if the table is occupied
        if (occupied)
        {
            // either it is occupied by an object that is not a container and as a result our object cannot be put down
            if (!(putItem is ContainerItem))
                throw new UnauthorizedActionException("Table is already occupied!");

            // either it is occupied by a container
            // In that case, the item we're manipulating can either be a foodItem that we add directly
            if (item is FoodItem)
            {
                ((ContainerItem)putItem).AddIngredient((FoodItem)item, this);
                return null;
            }

            // Or a container Item, in which case we operate a transfer between the two containers.
            if (item is ContainerItem heldContainer)
            {
                ContainerItem putContainer = (ContainerItem)putItem;

                // In that case, we need to return the updated held container in order for the carrier to pick it back up
                return TransferBetweenContainers(putContainer, heldContainer, carrier);
            }

            // Or we cannot put down the ingredient
            throw new UnauthorizedActionException("What do you think you're doing?");
        }

        // if the table is not occupied, we just drop the item on the table and make it occupied
        occupied = !occupied;
        putItem = item;
        item.GetDroppedOnTable();
        item.transform.position = transform.position + new Vector3(0, SizeConstants.TABLE_HEIGHT, 0);
        return null;
    }

    /// <summary>
    /// A function to pick up an item on the table
    /// </summary>
    /// <param name="carrier">The carrier picking up the object</param>
    /// <returns>The item being picked up</returns>
    public virtual Item PickUp(GameObject carrier)
    {
        // if the table is on fire, we can't interact with it
        if (onFire)
            throw new UnauthorizedActionException("Holy sh*t the table is on fire!!!!");
        // if it is not occupied, we can't pick up an object
        if (!occupied)
            throw new UnauthorizedActionException("Table is already empty!");

        // we empty the table and the item gets picked up
        occupied = !occupied;
        Item pickedUpItem = putItem;
        putItem = null;
  
        pickedUpItem.GetPickedUp(carrier);
        return pickedUpItem;
    }

    /// <summary>
    /// A function to transfer items between containers
    /// </summary>
    /// <param name="putContainer">The container that is already on the table</param>
    /// <param name="heldContainer">The container that was held by the carrier and is being put down</param>
    /// <param name="carrier">The object that was carrying the held container</param>
    /// <returns>The updated container being held</returns>
    public Item TransferBetweenContainers(ContainerItem putContainer, ContainerItem heldContainer, GameObject carrier)
    {
        // If there are both empty, we can't do a thing
        if ((heldContainer.content == null) && (putContainer.content == null))
            throw new UnauthorizedActionException("What are you trying to do ?");

        // If the heldContainer is empty, we fill it with the content putContainer
        if (heldContainer.content == null)
        {
            List<Ingredient> all_ingredients = RecipeAssembler.Disassemble((Recipe)putContainer.content);
            ContainerItem newHeldContainer = heldContainer.AddIngredient(all_ingredients, carrier: carrier);
            putItem = putContainer.Empty();
            return newHeldContainer;
        }

        // If the putContainer is empty, we fill it with the content heldContainer
        if (putContainer.content == null)
        {
            List<Ingredient> all_ingredients = RecipeAssembler.Disassemble((Recipe)heldContainer.content);
            putContainer.AddIngredient(all_ingredients, this);
            return heldContainer.Empty(carrier);
        }

        // If neither are empty : 
        //  - either both are cooking Container and we don't allow the transfer
        if ((heldContainer is CookingContainerItem) && (putContainer is CookingContainerItem))
            throw new UnauthorizedActionException("You can't do that sorry");

        //  - either the heldContainer is a cookingContainer and we transfer its content into the putContainer
        if (heldContainer is CookingContainerItem)
        {
            List<Ingredient> all_ingredients = RecipeAssembler.Disassemble((Recipe)heldContainer.content);
            putContainer.AddIngredient(all_ingredients, this);
            return heldContainer.Empty(carrier);
        }

        // - either the putContainer is a cookingContainer and we transfer its content into the heldContainer
        if (putContainer is CookingContainerItem)
        {
            List<Ingredient> all_ingredients = RecipeAssembler.Disassemble((Recipe)putContainer.content);
            ContainerItem newHeldContainer = heldContainer.AddIngredient(all_ingredients, carrier: carrier);
            putItem = putContainer.Empty();
            return newHeldContainer;
        }

        // - either none are cookingContainer and we transfer from the least full to the fullest (in case of equality, we transfer from the held to the put)
        List<Ingredient> putContainerIngredients = RecipeAssembler.Disassemble((Recipe)putContainer.content);
        List<Ingredient> heldContainerIngredients = RecipeAssembler.Disassemble((Recipe)heldContainer.content);

        if (putContainerIngredients.Count >= heldContainerIngredients.Count)
        {
            List<Ingredient> all_ingredients = RecipeAssembler.Disassemble((Recipe)heldContainer.content);
            putContainer.AddIngredient(all_ingredients, this);
            return heldContainer.Empty(carrier);
        }
        else
        {
            List<Ingredient> all_ingredients = RecipeAssembler.Disassemble((Recipe)putContainer.content);
            ContainerItem newHeldContainer = heldContainer.AddIngredient(all_ingredients, carrier: carrier);
            putItem = putContainer.Empty();
            return newHeldContainer;
        }
    }

    /// <summary>
    /// A function to set a table on fire 
    /// </summary>
    public void CatchFire()
    {
        // if the table is already on fire, the table cannot catch fire again
        if (onFire)
            throw new System.Exception("it's already on fire!");

        // if there is an item, it is burnt
        if (!(putItem is null))
            putItem = putItem.Burn();

        // Then we set the table on fire
        onFire = true;
        _burningFire = Instantiate(firePrefab, transform.position + new Vector3(0, SizeConstants.TABLE_HEIGHT, 0), transform.rotation);

        // Finally, we start a thread calling propagating the fire to other tables until extinction of the fire
        StartCoroutine(PropagationThroughTime());

    }

    /// <summary>
    /// A function to turn off the fire if the table is burning
    /// </summary>
    public void Extinguish()
    {
        // if the table is not on fire, we can't extinguish any fire
        if (!onFire)
            throw new System.Exception("What do you think you're doing?");

        // We destroy the fire object and set the table as not on fire
        Destroy(_burningFire);
        _burningFire = null;
        onFire = false;
    }

    /// <summary>
    /// A function to be executed by the coroutine to propagate the fire from this table to others every  amount of time while the table is on fire
    /// </summary>
    /// <returns>An IEnuerator to be executed by the coroutine</returns>
    public IEnumerator PropagationThroughTime()
    {
        // while the table is on fire
        while (onFire)
        {
            // we wait a certain amount of time and then propagate the fire (if is has not been extinguished in the mean time)
            yield return new WaitForSeconds(TimeConstants.TIME_BETWEEN_FIRE_PROPAGATION);
            if (onFire)
                Propagate();
        }
        yield return null;
    }

    /// <summary>
    /// A function propagating the fire from this table to another 
    /// </summary>
    public void Propagate()
    {
        // if the table is on fire, the fire can try to propagate to another table
        if (onFire)
        {
            // Then we check if there is a table nearby and if there is, it catches fire 
            Table candidate = PropagableTable();
            if (!(candidate is null))
            {
                (candidate).CatchFire();
            }

        }
    }

    /// <summary>
    /// A function to get a table to which the fire can propagate if there is one
    /// </summary>
    /// <returns></returns>
    public Table PropagableTable()
    {
        // We create a list and for each table in the adjacent table list, we add them to the list if there are not on fire
        List<Table> candidates = new List<Table>();
        foreach (Table table in adjacentTables)
        {
            if (!table.onFire)
                candidates.Add(table);
        }

        // If there are such tables, we select one of them randomly and return it 
        if (candidates.Count > 0)
            return candidates[(new System.Random()).Next(0, candidates.Count)];
        // If there isn't we return a null object
        return null;
    }



    /// <summary>
    /// A function to find all the adjacent tables (the table the fire can propagate to)
    /// </summary>
    /// <returns>A list of the tables that can be reached by fire</returns>
    public List<Table> FindAdjacentTables()
    {
        // We use an overlapping sphere to get all the colliders in a certain radius 
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 0.65f);

        // We create a list and for each of the colliders, if they correspound to a table we add it to the list.
        List<Table> result = new List<Table>();
        foreach (Collider coll in hitColliders)
        {
            Table temp = coll.gameObject.GetComponent<Table>();
            if (!(temp is null))
                result.Add(temp);
        }
        //Finally we return the list
        return result;
    }

}