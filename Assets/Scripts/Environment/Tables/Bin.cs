﻿using UnityEngine;

/// <summary>
/// This class implements the bin logic as an automatic action table : everything object put down on the bin will be thrown away or emptied
/// </summary>
public class Bin : AutomaticActionTable
{
    // The Audiosource for the emptying sound when the bin is used
    private AudioSource _emptyingSound;


    // Interaction related properties : we higlight the bin when it is in focus
    private Renderer rend;
    private Color colorBase = new Color(0.5f, 0.5f, 0.5f);
    private Color colorFocus = new Color(0.8f, 0.8f, 0.8f);
    
    // Before the items are instanciated, we link the reference to the actual audio source
    private void Awake()
    {
        _emptyingSound = GetComponent<AudioSource>();
    }

    /// <summary>
    /// The function putting down an oject for the bin
    /// </summary>
    /// <remarks>
    /// The object being deleted or emptied, we don't want it to be actually put down before executing the table action, hence the overridding
    /// </remarks>
    /// <param name="item">The item being put down</param>
    /// <param name="carrier">The carrier putting down the object, null if there is none</param>
    /// <returns>The emptied container if the item was a container, null if not</returns>
    public override Item PutDown(Item item, GameObject carrier = null)
    {
        // We just execute the table task and return its result
        return ExecuteTask(item, carrier);
    }

    /// <summary>
    /// This function is used to higlight the bin if it is in focus
    /// </summary>
    /// <param name="Switch">A boolean indicating whether the table is in focus</param>
    /// <remarks>
    /// Due to the different nature of the bin prefab, we need to overwrite this method for it to work properly for the bin as well
    /// </remarks>
    public override void Highlight(bool Switch)
    {
        rend = transform.gameObject.GetComponent<Renderer>();
        Material tableTopMaterial = rend.materials[0];
        if (Switch)
            tableTopMaterial.color = colorFocus;
        else
            tableTopMaterial.color = colorBase;
    }

    /// <summary>
    /// The task of throwing away/emptying being executed by the bin
    /// </summary>
    /// <param name="item">The item being is thrown away/amptied</param>
    /// <param name="carrier">The carrier putting down the object, null if there is none</param>
    /// <returns>The emptied container if the item was a container, null if not</returns>
    public override Item ExecuteTask(Item item, GameObject carrier)
    {
        // If the object is a fooditem, we destroy the object, play the emptying sound and return nothing
        if (item is FoodItem)
        {
            Destroy(item.gameObject);
            _emptyingSound.Play();
            return null;
        }
        // It the object is a container item, we empty it, play the emptying sound and return the new empty objet
        if (item is ContainerItem)
        {
            ContainerItem empty = ((ContainerItem)item).Empty(carrier);
            _emptyingSound.Play();
            return empty;
        }
        // If it is not a food item or a container, it cannot be thown away and we throw an exception
        throw new UnauthorizedActionException("You can't throw that away!");
    }
}