﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Objects of this class are represented by a Slider,
/// and indicate the progression of an action
/// </summary>
public class ActionProgression : MonoBehaviour
{
    // The Slider that represents the progression of the action
    public Slider slider;
    
    /// <summary>
    /// Updates the progression of the action by a given increment
    /// </summary>
    /// <param name="value">The increment to augment the progression</param>
    public void UpdateProgression(int value)
    {
        slider.value += value;
    }
}
