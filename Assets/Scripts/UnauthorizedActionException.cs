﻿using System;

/// <summary>
///  A class creating a customized exception.
///  This exception is thrown when the player tries to realize an action that is no possible or allowed.
/// </summary>
/// <remarks>
///  We use the class as a way to differentiate from the global exception
///  and trigger a special sound signifying to the player that he is trying to do something that is not allowed
///  </remarks>
public class UnauthorizedActionException : Exception
{
    public UnauthorizedActionException() { }

    public UnauthorizedActionException(string message) : base(message) { }

    public UnauthorizedActionException(string message, Exception inner) : base(message, inner) { }

}
